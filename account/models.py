from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings
from django.contrib.sessions.models import Session


class User(AbstractUser):
    phone = models.CharField(verbose_name='phone', max_length=255, unique=True)
    country = models.CharField(verbose_name='country', max_length=100)
    REQUIRED_FIELDS = ['username', 'first_name', 'last_name', 'email', 'last_name']
    USERNAME_FIELD = 'phone'

    class Meta:
        db_table = 'user'

    def get_username(self):
        return self


class Account(models.Model):
    username = models.CharField(max_length=100)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    phone_no = models.CharField(max_length=20)
    customer_id = models.CharField(max_length=10)
    bal = models.CharField(max_length=200)
    usd_bal = models.FloatField()
    gbp_bal = models.FloatField()
    eur_bal = models.FloatField()
    status = models.CharField(max_length=100)

    class Meta:
        db_table = "account"


class Transactions(models.Model):
    sender = models.CharField(max_length=50)
    receiver = models.CharField(max_length=50)
    description = models.CharField(max_length=100)
    amount = models.FloatField(max_length=200)
    ref_no = models.CharField(max_length=40)
    date = models.DateField(auto_now=True)
    cur = models.CharField(max_length=10)
    charges = models.FloatField()

    class Meta:
        db_table = "transactions"


class Voucher(models.Model):
    v_creator = models.CharField(max_length=100)
    v_code = models.CharField(max_length=20)
    v_amount = models.CharField(max_length=200)
    ref_no = models.CharField(max_length=100)
    v_status = models.CharField(max_length=10)
    v_loader = models.CharField(max_length=100)
    v_date_load = models.CharField(max_length=100)
    v_date = models.DateField(auto_now=True)

    class Meta:
        db_table = "voucher"


class Ticket(models.Model):
    subject = models.CharField(max_length=500)
    category = models.CharField(max_length=100)
    owner = models.CharField(max_length=100)
    content = models.CharField(max_length=1000)
    priority = models.CharField(max_length=100)
    status = models.CharField(max_length=20)
    ticket_id = models.CharField(max_length=10)
    date_created = models.DateField(auto_now=True)
    date_action = models.CharField(max_length=100)

    class Meta:
        db_table = "ticket"


class Merchant(models.Model):
    bus_owner_username = models.CharField(max_length=100)
    bus_name = models.CharField(max_length=200)
    bus_address = models.CharField(max_length=200)
    bus_email = models.CharField(max_length=100)
    bus_no = models.CharField(max_length=20)
    bus_website = models.CharField(max_length=200)
    api_test_key = models.CharField(max_length=200)
    api_live_key = models.CharField(max_length=200)
    bus_logo = models.FileField(upload_to='media')
    int_mode = models.CharField(max_length=100)
    w_p_charges = models.CharField(max_length=100)

    class Meta:
        db_table = "merchant"


class MerchantPayment(models.Model):
    bus_owner_username = models.CharField(max_length=100)
    payee = models.CharField(max_length=100)
    amount = models.CharField(max_length=200)

    class Meta:
        db_table = "merchantPayment"


class Bank(models.Model):
    username = models.CharField(max_length=100)
    account_name = models.CharField(max_length=100)
    account_no = models.CharField(max_length=100)
    code = models.CharField(max_length=10)
    bank_name = models.CharField(max_length=20)

    class Meta:
        db_table = "bank"


class Withdraw(models.Model):
    username = models.CharField(max_length=100)
    amount = models.CharField(max_length=100)
    acct_name = models.CharField(max_length=200)
    acct_no = models.CharField(max_length=200)
    bank_name = models.CharField(max_length=100)
    ref_no = models.CharField(max_length=100)
    status = models.CharField(max_length=100)
    date = models.DateField(auto_now=True)

    class Meta:
        db_table = "withdraw"


class Invoice(models.Model):
    sender = models.CharField(max_length=100)
    receiver = models.CharField(max_length=100)
    amount = models.CharField(max_length=100)
    content = models.CharField(max_length=500)
    date = models.DateField(auto_now=True)
    due_date = models.CharField(max_length=100)
    acct_no = models.CharField(max_length=100)
    acct_name = models.CharField(max_length=100)
    bank_name = models.CharField(max_length=100)
    bank_code = models.CharField(max_length=100)
    checkout_url = models.TextField()
    cust_name = models.TextField()
    sender_name = models.TextField()
    inv_ref = models.CharField(max_length=100)
    status = models.CharField(max_length=10)
    date_paid = models.CharField(max_length=100)
    action = models.CharField(max_length=50)

    class Meta:
        db_table = "invoice"


class Subscription(models.Model):
    email = models.CharField(max_length=200)
    date_reg = models.DateField(auto_now=True)

    class Meta:
        db_table = "subscription"


class Banks(models.Model):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=100)

    class Meta:
        db_table = "banks"


class VirtualCard(models.Model):
    card_user = models.CharField(max_length=100)
    card_no = models.CharField(max_length=100)
    pin = models.CharField(max_length=10)
    card_bal = models.CharField(max_length=1000)

    class Meta:
        db_table = "virtual_card"


class Customer(models.Model):
    merchant_id = models.IntegerField()
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    email = models.EmailField()
    phone_no = models.CharField(max_length=100)

    class Meta:
        db_table = 'customer'


class BulkPayment(models.Model):
    merchant_id = models.IntegerField()
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    email = models.EmailField()
    phone_no = models.CharField(max_length=100)
    amount = models.FloatField()

    class Meta:
        db_table = 'bulk_payment'



class UserSession(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    session = models.OneToOneField(Session, on_delete=models.CASCADE)

    class Meta:
        db_table = 'user_session'


class PaymentGroup(models.Model):
    group_creator_id = models.IntegerField()
    group_name = models.CharField(max_length=255)
    date_created = models.CharField(max_length=100)

    class Meta:
        db_table = 'payment_group'


class GroupMember(models.Model):
    group_id = models.IntegerField()
    group_creator_id = models.IntegerField()
    mobile_no = models.CharField(max_length=100)
    date_added = models.CharField(max_length=100)

    class Meta:
        db_table = 'group_member'


class ReservedAccount(models.Model):
    acct_owner_id = models.IntegerField()
    acct_number = models.CharField(max_length=100)
    acct_name = models.CharField(max_length=255)
    bank_name = models.CharField(max_length=100)
    bank_code = models.CharField(max_length=50)
    currency = models.CharField(max_length=10)
    reserve_ref = models.CharField(max_length=100)
    acct_type = models.CharField(max_length=100)
    user_name = models.CharField(max_length=100)
    acct_ref = models.CharField(max_length=100)
    c_email = models.EmailField()
    status = models.CharField(max_length=100)


    class Meta:
        db_table = 'reserved_account'


class Kyc(models.Model):
    c_username = models.CharField(max_length=255)
    address = models.TextField()
    idm = models.CharField(max_length=50)
    idno = models.CharField(max_length=255)
    mid = models.FileField(upload_to='media')
    status = models.IntegerField()

    class Meta:
        db_table = 'kyc'


class Utility(models.Model):
    service_name = models.CharField(max_length=100)
    service_id = models.CharField(max_length=100)
    variation_code = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    variation_amount = models.FloatField()

    class Meta:
        db_table = 'utility'


class Electricity(models.Model):
    owner_username = models.CharField(max_length=100)
    txn_id = models.CharField(max_length=100)
    txn_ref = models.CharField(max_length=100)
    desc = models.CharField(max_length=100)
    token = models.CharField(max_length=100)
    date_purchase = models.CharField(max_length=100)

    class Meta:
        db_table = 'electricity'


class Countries(models.Model):
    iso = models.TextField()
    name = models.TextField()
    nicename = models.TextField()
    iso3 = models.TextField()
    numcode = models.CharField(max_length=10)
    phonecode = models.CharField(max_length=10)

    class Meta:
        db_table = 'countries'


