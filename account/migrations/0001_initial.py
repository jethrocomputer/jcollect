# Generated by Django 3.0.3 on 2020-05-18 09:46

from django.conf import settings
import django.contrib.auth.models
import django.contrib.auth.validators
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('sessions', '0001_initial'),
        ('auth', '0011_update_proxy_permissions'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(error_messages={'unique': 'A user with that username already exists.'}, help_text='Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.', max_length=150, unique=True, validators=[django.contrib.auth.validators.UnicodeUsernameValidator()], verbose_name='username')),
                ('first_name', models.CharField(blank=True, max_length=30, verbose_name='first name')),
                ('last_name', models.CharField(blank=True, max_length=150, verbose_name='last name')),
                ('email', models.EmailField(blank=True, max_length=254, verbose_name='email address')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('phone', models.CharField(max_length=255, unique=True, verbose_name='phone')),
                ('country', models.CharField(max_length=100, verbose_name='country')),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'db_table': 'user',
            },
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Account',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(max_length=100)),
                ('first_name', models.CharField(max_length=100)),
                ('last_name', models.CharField(max_length=100)),
                ('phone_no', models.CharField(max_length=20)),
                ('customer_id', models.CharField(max_length=10)),
                ('bal', models.CharField(max_length=200)),
                ('usd_bal', models.FloatField()),
                ('gbp_bal', models.FloatField()),
                ('eur_bal', models.FloatField()),
                ('status', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'account',
            },
        ),
        migrations.CreateModel(
            name='Bank',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(max_length=100)),
                ('account_name', models.CharField(max_length=100)),
                ('account_no', models.CharField(max_length=100)),
                ('code', models.CharField(max_length=10)),
                ('bank_name', models.CharField(max_length=20)),
            ],
            options={
                'db_table': 'bank',
            },
        ),
        migrations.CreateModel(
            name='Banks',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('code', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'banks',
            },
        ),
        migrations.CreateModel(
            name='BulkPayment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('merchant_id', models.IntegerField()),
                ('first_name', models.CharField(max_length=200)),
                ('last_name', models.CharField(max_length=200)),
                ('email', models.EmailField(max_length=254)),
                ('phone_no', models.CharField(max_length=100)),
                ('amount', models.FloatField()),
            ],
            options={
                'db_table': 'bulk_payment',
            },
        ),
        migrations.CreateModel(
            name='Countries',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('iso', models.TextField()),
                ('name', models.TextField()),
                ('nicename', models.TextField()),
                ('iso3', models.TextField()),
                ('numcode', models.CharField(max_length=10)),
                ('phonecode', models.CharField(max_length=10)),
            ],
            options={
                'db_table': 'countries',
            },
        ),
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('merchant_id', models.IntegerField()),
                ('first_name', models.CharField(max_length=200)),
                ('last_name', models.CharField(max_length=200)),
                ('email', models.EmailField(max_length=254)),
                ('phone_no', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'customer',
            },
        ),
        migrations.CreateModel(
            name='Electricity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('owner_username', models.CharField(max_length=100)),
                ('txn_id', models.CharField(max_length=100)),
                ('txn_ref', models.CharField(max_length=100)),
                ('desc', models.CharField(max_length=100)),
                ('token', models.CharField(max_length=100)),
                ('date_purchase', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'electricity',
            },
        ),
        migrations.CreateModel(
            name='GroupMember',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('group_id', models.IntegerField()),
                ('group_creator_id', models.IntegerField()),
                ('mobile_no', models.CharField(max_length=100)),
                ('date_added', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'group_member',
            },
        ),
        migrations.CreateModel(
            name='Invoice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sender', models.CharField(max_length=100)),
                ('receiver', models.CharField(max_length=100)),
                ('amount', models.CharField(max_length=100)),
                ('content', models.CharField(max_length=500)),
                ('date', models.DateField(auto_now=True)),
                ('due_date', models.CharField(max_length=100)),
                ('acct_no', models.CharField(max_length=100)),
                ('acct_name', models.CharField(max_length=100)),
                ('bank_name', models.CharField(max_length=100)),
                ('bank_code', models.CharField(max_length=100)),
                ('checkout_url', models.TextField()),
                ('cust_name', models.TextField()),
                ('sender_name', models.TextField()),
                ('inv_ref', models.CharField(max_length=100)),
                ('status', models.CharField(max_length=10)),
                ('date_paid', models.CharField(max_length=100)),
                ('action', models.CharField(max_length=50)),
            ],
            options={
                'db_table': 'invoice',
            },
        ),
        migrations.CreateModel(
            name='Kyc',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('c_username', models.CharField(max_length=255)),
                ('address', models.TextField()),
                ('idm', models.CharField(max_length=50)),
                ('idno', models.CharField(max_length=255)),
                ('mid', models.FileField(upload_to='media')),
                ('status', models.IntegerField()),
            ],
            options={
                'db_table': 'kyc',
            },
        ),
        migrations.CreateModel(
            name='Merchant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bus_owner_username', models.CharField(max_length=100)),
                ('bus_name', models.CharField(max_length=200)),
                ('bus_address', models.CharField(max_length=200)),
                ('bus_email', models.CharField(max_length=100)),
                ('bus_no', models.CharField(max_length=20)),
                ('bus_website', models.CharField(max_length=200)),
                ('api_test_key', models.CharField(max_length=200)),
                ('api_live_key', models.CharField(max_length=200)),
                ('bus_logo', models.FileField(upload_to='media')),
                ('int_mode', models.CharField(max_length=100)),
                ('w_p_charges', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'merchant',
            },
        ),
        migrations.CreateModel(
            name='MerchantPayment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bus_owner_username', models.CharField(max_length=100)),
                ('payee', models.CharField(max_length=100)),
                ('amount', models.CharField(max_length=200)),
            ],
            options={
                'db_table': 'merchantPayment',
            },
        ),
        migrations.CreateModel(
            name='PaymentGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('group_creator_id', models.IntegerField()),
                ('group_name', models.CharField(max_length=255)),
                ('date_created', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'payment_group',
            },
        ),
        migrations.CreateModel(
            name='ReservedAccount',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('acct_owner_id', models.IntegerField()),
                ('acct_number', models.CharField(max_length=100)),
                ('acct_name', models.CharField(max_length=255)),
                ('bank_name', models.CharField(max_length=100)),
                ('bank_code', models.CharField(max_length=50)),
                ('currency', models.CharField(max_length=10)),
                ('reserve_ref', models.CharField(max_length=100)),
                ('acct_type', models.CharField(max_length=100)),
                ('user_name', models.CharField(max_length=100)),
                ('acct_ref', models.CharField(max_length=100)),
                ('c_email', models.EmailField(max_length=254)),
                ('status', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'reserved_account',
            },
        ),
        migrations.CreateModel(
            name='Subscription',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.CharField(max_length=200)),
                ('date_reg', models.DateField(auto_now=True)),
            ],
            options={
                'db_table': 'subscription',
            },
        ),
        migrations.CreateModel(
            name='Ticket',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('subject', models.CharField(max_length=500)),
                ('category', models.CharField(max_length=100)),
                ('owner', models.CharField(max_length=100)),
                ('content', models.CharField(max_length=1000)),
                ('priority', models.CharField(max_length=100)),
                ('status', models.CharField(max_length=20)),
                ('ticket_id', models.CharField(max_length=10)),
                ('date_created', models.DateField(auto_now=True)),
                ('date_action', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'ticket',
            },
        ),
        migrations.CreateModel(
            name='Transactions',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sender', models.CharField(max_length=50)),
                ('receiver', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=100)),
                ('amount', models.FloatField(max_length=200)),
                ('ref_no', models.CharField(max_length=40)),
                ('date', models.DateField(auto_now=True)),
                ('cur', models.CharField(max_length=10)),
                ('charges', models.FloatField()),
            ],
            options={
                'db_table': 'transactions',
            },
        ),
        migrations.CreateModel(
            name='Utility',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('service_name', models.CharField(max_length=100)),
                ('service_id', models.CharField(max_length=100)),
                ('variation_code', models.CharField(max_length=100)),
                ('name', models.CharField(max_length=100)),
                ('variation_amount', models.FloatField()),
            ],
            options={
                'db_table': 'utility',
            },
        ),
        migrations.CreateModel(
            name='VirtualCard',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('card_user', models.CharField(max_length=100)),
                ('card_no', models.CharField(max_length=100)),
                ('pin', models.CharField(max_length=10)),
                ('card_bal', models.CharField(max_length=1000)),
            ],
            options={
                'db_table': 'virtual_card',
            },
        ),
        migrations.CreateModel(
            name='Voucher',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('v_creator', models.CharField(max_length=100)),
                ('v_code', models.CharField(max_length=20)),
                ('v_amount', models.CharField(max_length=200)),
                ('ref_no', models.CharField(max_length=100)),
                ('v_status', models.CharField(max_length=10)),
                ('v_loader', models.CharField(max_length=100)),
                ('v_date_load', models.CharField(max_length=100)),
                ('v_date', models.DateField(auto_now=True)),
            ],
            options={
                'db_table': 'voucher',
            },
        ),
        migrations.CreateModel(
            name='Withdraw',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(max_length=100)),
                ('amount', models.CharField(max_length=100)),
                ('acct_name', models.CharField(max_length=200)),
                ('acct_no', models.CharField(max_length=200)),
                ('bank_name', models.CharField(max_length=100)),
                ('ref_no', models.CharField(max_length=100)),
                ('status', models.CharField(max_length=100)),
                ('date', models.DateField(auto_now=True)),
            ],
            options={
                'db_table': 'withdraw',
            },
        ),
        migrations.CreateModel(
            name='UserSession',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('session', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='sessions.Session')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'user_session',
            },
        ),
    ]
