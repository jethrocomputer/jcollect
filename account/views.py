from django.db.models import Q
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.contrib.auth import get_user_model, authenticate, login as dj_login, logout as s_logout
from django.contrib.auth.decorators import login_required
from .models import Account, Transactions, Voucher, Ticket, Merchant, Bank, Withdraw, Invoice, Banks, VirtualCard
from .models import *
from super.models import Resolution, Settings, Commission
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.core.paginator import Paginator
from django.utils.html import strip_tags
from django.db.models import Sum
from wallet.settings import EMAIL_FROM
from typing import Union
from collections import namedtuple
import random
import string
import uuid
import datetime
from datetime import datetime
import csv
import io
from django.contrib.auth import user_logged_in
from django.dispatch.dispatcher import receiver
import requests
import json, copy
from django.http import HttpResponse
from django.core import serializers
from django.views.decorators.http import require_POST
from django.views.decorators.csrf import csrf_exempt
UserModel = get_user_model()
from django.utils.crypto import get_random_string



# Login view function that handle all the login
def login(request):
    if request.user.is_authenticated:
        return redirect('dashboard')
    if request.method == 'POST':
        phone = request.POST['phone']
        password = request.POST['password']

        user = authenticate(phone=phone, password=password)

        if user is not None:
            dj_login(request, user)
            #request.session.set_expiry(300)
            return redirect('dashboard')
        else:
            messages.error(request, 'Invalid Credentials')
            return redirect('login')
    else:
        return render(request, 'login.html')


# Registration View Function that handle all the Registration
def register(request):
    N = 5
    res = ''.join(random.choices(string.digits, k=N))
    cus_id = str(res)
    if request.method == 'POST':
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        phone_no = request.POST['phone_no']
        email = request.POST['email']
        password1 = request.POST['password1']
        password2 = request.POST['password2']
        country = request.POST['country']
        c_code = request.POST['c_code']

        newPhone = c_code + phone_no

        if password1 == password2:
            if UserModel.objects.filter(email=email).exists():
                messages.error(request, 'Email Taken')
                return redirect('register')
            elif UserModel.objects.filter(phone=newPhone).exists():
                messages.error(request, 'Mobile Number Used')
                return redirect('register')
            else:
                if country == "NG":
                    user = UserModel.objects.create_user(username=last_name+cus_id,
                                                     password=password1,
                                                     email=email,
                                                     first_name=first_name,
                                                     last_name=last_name,
                                                     phone=newPhone,
                                                     country=country,
                                                     )
                    users = Account(username=last_name+cus_id,
                                first_name=first_name,
                                last_name=last_name,
                                phone_no=newPhone,
                                customer_id=cus_id,
                                bal='0',
                                eur_bal='0',
                                gbp_bal='0',
                                usd_bal='0',
                                status='hold')
                    users.save()
                    user.save()
                    c_user_id = UserModel.objects.values('id').get(phone=newPhone)['id']

                    url = "https://sandbox.monnify.com/api/v1/auth/login"

                    payload = {}
                    headers = {
                    'Authorization': 'Basic TUtfVEVTVF84VUJYR0tURlNCOkVOUkM0RkRZS1NUVVlRS0E1M1lQWEJGTFVGWFdZSEcy'
                    }

                    response = requests.request("POST", url, headers=headers, data = payload)

                    response_dict = json.loads(response.text)
                    h = []
                    for i in response_dict:
                        data = response_dict[i]
                        h.append(data)
                    e = []
                    for r in h[3]:
                        toke = h[3][r]
                        e.append(toke)
                    a = "Bearer"+ " " + e[0]

                    url = "https://sandbox.monnify.com/api/v1/bank-transfer/reserved-accounts"

                    payload = '{"accountReference\": \"'+ newPhone +'\",   \"accountName\": \"'+ first_name + " " + last_name +'\",\n    \"currencyCode\": \"NGN\",\n    \"contractCode\": \"2917634474\",\n    \"customerEmail\": \"'+ email +'\"\n}'
                    headers = {
                    'Content-Type': 'application/json',
                    'Authorization': a
                    }

                    response = requests.request("POST", url, headers=headers, data = payload)
                    r_dict = json.loads(response.text)
                    resv = []
                    for j in r_dict:
                        data = r_dict[j]
                        resv.append(data)
                    res = resv[3]
                    #print(res['contractCode'])
                    #print(res)
                    acct_no = res['accountNumber']
                    bank_name = res['bankName']
                    bank_code = res['bankCode']
                    reserve_ref = res['reservationReference']
                    acct_type = res['reservedAccountType']
                    acct_ref = res['accountReference']
                    acct_name = res['accountName']
                    c_email = res['customerEmail']
                    status = res['status']
                    cur = res['currencyCode']

                    acct_save = ReservedAccount(acct_owner_id=c_user_id, acct_number=acct_no, acct_name=acct_name, bank_name=bank_name, bank_code=bank_code,
                    currency=cur, reserve_ref=reserve_ref, acct_type=acct_type, user_name=last_name+cus_id, acct_ref=acct_ref, c_email=c_email, status=status)
                    acct_save.save()
                    # messages.info(request, 'Account Created Successfully, Kindly login with Your Username and Password')
                    subject, from_email, to = 'Account Registration', EMAIL_FROM, email
                    html_content = render_to_string('mail/signup.html',
                                            {'first_name': first_name, 'cus_id': cus_id, 'username': phone_no})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    user = authenticate(phone=newPhone, password=password1)
                    dj_login(request, user)
                    return redirect('dashboard')
                else:
                    user = UserModel.objects.create_user(username=last_name+cus_id,
                                                     password=password1,
                                                     email=email,
                                                     first_name=first_name,
                                                     last_name=last_name,
                                                     phone=newPhone,
                                                     country=country,
                                                     )
                    users = Account(username=last_name+cus_id,
                                first_name=first_name,
                                last_name=last_name,
                                phone_no=newPhone,
                                customer_id=cus_id,
                                bal='0',
                                eur_bal='0',
                                gbp_bal='0',
                                usd_bal='0',
                                status='hold')
                    users.save()
                    user.save()
                    subject, from_email, to = 'Account Registration', EMAIL_FROM, email
                    html_content = render_to_string('mail/signup.html',
                                            {'first_name': first_name, 'cus_id': cus_id, 'username': phone_no})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    user = authenticate(phone=newPhone, password=password1)
                    dj_login(request, user)
                    return redirect('dashboard')
        else:
            messages.info(request, 'Password not Matching')
            return redirect('register')

    else:
        nation = Countries.objects.filter()
        return render(request, 'register.html', {'nation': nation})


# Dashboard view function
@login_required(login_url='login')
def dashboard(request):
    c_user = request.user.username
    c_user_id = request.user.id
    show = Account.objects.all().get(username=c_user)
    shows = VirtualCard.objects.filter(card_user=c_user)
    card_check = VirtualCard.objects.filter(card_user=c_user).exists()
    tran = Transactions.objects.filter(Q(sender=c_user) | Q(receiver=c_user)).order_by('-id')[:3]
    user_exists = ReservedAccount.objects.filter(acct_owner_id=c_user_id).exists()
    user_acct = ReservedAccount.objects.filter(acct_owner_id=c_user_id)
    # check = VirtualCard.objects.values('card_user').get(card_user=c_user)['card_user']
    vendor = Transactions.objects.filter(receiver=c_user).filter(
        description='Merchant').aggregate(Sum('amount'))['amount__sum']
    context = {'show': show, 'vendor': vendor, 'shows': shows, 'tran': tran, 'card_check': card_check, 'user_exists':user_exists, 'user_acct':user_acct}
    # print(float(round(13.500000000000002, 2)))
    return render(request, 'dashboard.html', context)


# Logout function the terminate all the login session
def logout(request):
    s_logout(request)
    messages.success(request, "You have successfully logout")
    return redirect('login')


# Transfer function that handle the transfer of money within the system
@login_required(login_url='login')
def transfer(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    ref_no = uuid.uuid4().hex[:10].upper()
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    c_user = request.user.username
    if request.method == 'POST':
        s_username = request.POST['s_username']
        r_number = request.POST['r_number']
        amount = request.POST['amount']
        r_cur = request.POST['r_cur']
        s_cur = request.POST['s_cur']
        r_bal = Account.objects.all().get(phone_no=r_number)
        s_bal = Account.objects.all().get(username=s_username)
        first_name = Account.objects.values('first_name').get(username=s_username)['first_name']
        r_first_name = Account.objects.values('first_name').get(phone_no=r_number)['first_name']
        r_mail = User.objects.values('email').get(username=s_username)['email']
        r_username = Account.objects.values('username').get(phone_no=r_number)['username']
        m_email = User.objects.values('email').get(username=r_username)['email']
        show = Account.objects.values().get(phone_no=r_number)['username']
        status = Account.objects.values('status').get(username=s_username)['status']
        r_status = Account.objects.values('status').get(phone_no=r_number)['status']
        charge = Commission.objects.values('transfer').get(id=1)['transfer']

        # print("Hello: ", show)
        
        if status == 'hold':
            messages.warning(request, 'Your Account is on Hold, Please contact our agent')
        elif r_status == 'hold':
            messages.warning(request, "This Customer Can't receive Money at the Moment")
        elif s_username == show:
            messages.warning(request, "You Can't Send Money to Yourself!!")
        else:
            if s_cur == "NGN" and r_cur == "NGN":

                f_charge = (float(charge))
                sb = (float(s_bal.bal))
                rb = (float(r_bal.bal))
                am = (float(amount))
                c_amt = am * (f_charge / 100)
                if am > sb:
                    messages.warning(request, 'Insufficient Balance')
                    return redirect('transfer')
                else:
                    new = rb + am
                    Account.objects.filter(phone_no=r_number).update(bal=new)

                    new_2 = (sb - (am + c_amt))
                    Account.objects.filter(username=s_username).update(bal=new_2)

                    trans = Transactions(sender=s_username,
                                        receiver=show,
                                        amount=amount,
                                        description=f'FT/NGN/{r_first_name}',
                                        ref_no=ref_no,
                                        cur='NGN',
                                        charges=c_amt)
                    trans.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, r_mail
                    html_content = render_to_string('mail/s_mail.html',
                                            {'first_name': first_name, 'new_2': new_2, 'ref_no': ref_no, 'amount': am,
                                             'receiver': r_number, 'c_amt': c_amt, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #         Receiver Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, m_email
                    html_content = render_to_string('mail/r_mail.html',
                                            {'r_first_name': r_first_name, 'new': new, 'ref_no': ref_no, 'amount': am,
                                             'sender': first_name, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return render(request, 'send-money-success.html', {'amount': am, 'r_first_name': r_first_name})
            elif s_cur == "NGN" and r_cur == "USD":
                f_charge = (float(charge))
                sb = (float(s_bal.bal))
                rb = (float(r_bal.usd_bal))
                amt = (float(amount))
                c_amt = amt * (f_charge / 100)
                if amt > sb:
                    messages.warning(request, 'Insufficient Balance')
                    return redirect('transfer')
                else:
                    url = 'https://fcsapi.com/api-v2/forex/converter?symbol=NGN/USD&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                    payload = {}
                    headers = {
                    'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                    }

                    response = requests.request("POST", url, headers=headers, data = payload)

                    #print(response.text.encode('utf8'))
                    r_dict = json.loads(response.text)
        
                    e = []
                    for i in r_dict:
                        dt = r_dict[i]
                        txn = e.append(dt)
                    trans = e[3]
                    total = trans['total']
                    t_amt = float(total)
                    new = rb + t_amt
                    new1 = (str(round(new, 1)))
                    Account.objects.filter(phone_no=r_number).update(usd_bal=new1)

                    new_2 = (sb - (amt + c_amt))
                    Account.objects.filter(username=s_username).update(bal=new_2)

                    trans = Transactions(sender=s_username,
                                        receiver=show,
                                        amount=new1,
                                        description=f'FT/USD/{r_first_name}',
                                        ref_no=ref_no,
                                        cur='USD',
                                        charges=c_amt)
                    trans.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, r_mail
                    html_content = render_to_string('mail/s_mail.html',
                                            {'first_name': first_name, 'new_2': new_2, 'ref_no': ref_no, 'amount': amt,
                                             'receiver': r_number, 'c_amt': c_amt, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #         Receiver Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, m_email
                    html_content = render_to_string('mail/r_mail.html',
                                            {'r_first_name': r_first_name, 'new': new, 'ref_no': ref_no, 'amount': amt,
                                             'sender': first_name, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return render(request, 'send-money-success.html', {'amount': amt, 'r_first_name': r_first_name})
            elif s_cur == "NGN" and r_cur == "GBP":
                f_charge = (float(charge))
                sb = (float(s_bal.bal))
                rb = (float(r_bal.gbp_bal))
                amt = (float(amount))
                c_amt = amt * (f_charge / 100)
                if amt > sb:
                    messages.warning(request, 'Insufficient Balance')
                    return redirect('transfer')
                else:
                    url = 'https://fcsapi.com/api-v2/forex/converter?symbol=NGN/GBP&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                    payload = {}
                    headers = {
                    'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                    }

                    response = requests.request("POST", url, headers=headers, data = payload)

                    #print(response.text.encode('utf8'))
                    r_dict = json.loads(response.text)
        
                    e = []
                    for i in r_dict:
                        dt = r_dict[i]
                        txn = e.append(dt)
                    trans = e[3]
                    total = trans['total']
                    t_amt = float(total)
                    new = rb + t_amt
                    new1 = (str(round(new, 1)))
                    Account.objects.filter(phone_no=r_number).update(gbp_bal=new1)

                    new_2 = (sb - (amt + c_amt))
                    Account.objects.filter(username=s_username).update(bal=new_2)

                    trans = Transactions(sender=s_username,
                                        receiver=show,
                                        amount=new1,
                                        description=f'FT/GBP/{r_first_name}',
                                        ref_no=ref_no,
                                        cur='GBP',
                                        charges=c_amt)
                    trans.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, r_mail
                    html_content = render_to_string('mail/s_mail.html',
                                            {'first_name': first_name, 'new_2': new_2, 'ref_no': ref_no, 'amount': amt,
                                             'receiver': r_number, 'c_amt': c_amt, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #         Receiver Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, m_email
                    html_content = render_to_string('mail/r_mail.html',
                                            {'r_first_name': r_first_name, 'new': new, 'ref_no': ref_no, 'amount': amt,
                                             'sender': first_name, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return render(request, 'send-money-success.html', {'amount': amt, 'r_first_name': r_first_name})
            elif s_cur == "NGN" and r_cur == "EUR":
                f_charge = (float(charge))
                sb = (float(s_bal.bal))
                rb = (float(r_bal.eur_bal))
                amt = (float(amount))
                c_amt = amt * (f_charge / 100)
                if amt > sb:
                    messages.warning(request, 'Insufficient Balance')
                    return redirect('transfer')
                else:
                    url = 'https://fcsapi.com/api-v2/forex/converter?symbol=NGN/EUR&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                    payload = {}
                    headers = {
                    'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                    }

                    response = requests.request("POST", url, headers=headers, data = payload)

                    #print(response.text.encode('utf8'))
                    r_dict = json.loads(response.text)
        
                    e = []
                    for i in r_dict:
                        dt = r_dict[i]
                        txn = e.append(dt)
                    trans = e[3]
                    total = trans['total']
                    t_amt = float(total)
                    new = rb + t_amt
                    new1 = (str(round(new, 1)))
                    Account.objects.filter(phone_no=r_number).update(eur_bal=new1)

                    new_2 = (sb - (amt + c_amt))
                    Account.objects.filter(username=s_username).update(bal=new_2)

                    trans = Transactions(sender=s_username,
                                        receiver=show,
                                        amount=new1,
                                        description=f'FT/EUR/{r_first_name}',
                                        ref_no=ref_no,
                                        cur='EUR',
                                        charges=c_amt)
                    trans.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, r_mail
                    html_content = render_to_string('mail/s_mail.html',
                                            {'first_name': first_name, 'new_2': new_2, 'ref_no': ref_no, 'amount': amt,
                                             'receiver': r_number, 'c_amt': c_amt, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #         Receiver Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, m_email
                    html_content = render_to_string('mail/r_mail.html',
                                            {'r_first_name': r_first_name, 'new': new, 'ref_no': ref_no, 'amount': amt,
                                             'sender': first_name, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return render(request, 'send-money-success.html', {'amount': amt, 'r_first_name': r_first_name})
            elif s_cur == "USD" and r_cur == "NGN":
                f_charge = (float(charge))
                sb = (float(s_bal.usd_bal))
                rb = (float(r_bal.bal))
                amt = (float(amount))
                c_amt = amt * (f_charge / 100)
                if amt > sb:
                    messages.warning(request, 'Insufficient Balance')
                    return redirect('transfer')
                else:
                    url = 'https://fcsapi.com/api-v2/forex/converter?symbol=USD/NGN&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                    payload = {}
                    headers = {
                    'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                    }

                    response = requests.request("POST", url, headers=headers, data = payload)

                    #print(response.text.encode('utf8'))
                    r_dict = json.loads(response.text)
        
                    e = []
                    for i in r_dict:
                        dt = r_dict[i]
                        txn = e.append(dt)
                    trans = e[3]
                    total = trans['total']
                    t_amt = float(total)
                    new = rb + t_amt
                    new1 = (str(round(new, 1)))
                    Account.objects.filter(phone_no=r_number).update(bal=new1)

                    new_2 = (sb - (amt + c_amt))
                    Account.objects.filter(username=s_username).update(usd_bal=new_2)

                    trans = Transactions(sender=s_username,
                                        receiver=show,
                                        amount=new1,
                                        description=f'FT/NGN/{r_first_name}',
                                        ref_no=ref_no,
                                        cur='NGN',
                                        charges=c_amt)
                    trans.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, r_mail
                    html_content = render_to_string('mail/s_mail.html',
                                            {'first_name': first_name, 'new_2': new_2, 'ref_no': ref_no, 'amount': amt,
                                             'receiver': r_number, 'c_amt': c_amt, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #         Receiver Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, m_email
                    html_content = render_to_string('mail/r_mail.html',
                                            {'r_first_name': r_first_name, 'new': new, 'ref_no': ref_no, 'amount': amt,
                                             'sender': first_name, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return render(request, 'send-money-success.html', {'amount': amt, 'r_first_name': r_first_name})
            elif s_cur == "USD" and r_cur == "USD":
                f_charge = (float(charge))
                sb = (float(s_bal.usd_bal))
                rb = (float(r_bal.usd_bal))
                am = (float(amount))
                c_amt = am * (f_charge / 100)
                if am > sb:
                    messages.warning(request, 'Insufficient Balance')
                    return redirect('transfer')
                else:
                    new = rb + am
                    Account.objects.filter(phone_no=r_number).update(usd_bal=new)

                    new_2 = (sb - (am + c_amt))
                    Account.objects.filter(username=s_username).update(usd_bal=new_2)

                    trans = Transactions(sender=s_username,
                                        receiver=show,
                                        amount=amount,
                                        description=f'FT/USD/{r_first_name}',
                                        ref_no=ref_no,
                                        cur='USD',
                                        charges=c_amt)
                    trans.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, r_mail
                    html_content = render_to_string('mail/s_mail.html',
                                            {'first_name': first_name, 'new_2': new_2, 'ref_no': ref_no, 'amount': am,
                                             'receiver': r_number, 'c_amt': c_amt, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #         Receiver Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, m_email
                    html_content = render_to_string('mail/r_mail.html',
                                            {'r_first_name': r_first_name, 'new': new, 'ref_no': ref_no, 'amount': am,
                                             'sender': first_name, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return render(request, 'send-money-success.html', {'amount': am, 'r_first_name': r_first_name})
            elif s_cur == "USD" and r_cur == "EUR":
                f_charge = (float(charge))
                sb = (float(s_bal.usd_bal))
                rb = (float(r_bal.eur_bal))
                amt = (float(amount))
                c_amt = amt * (f_charge / 100)
                if amt > sb:
                    messages.warning(request, 'Insufficient Balance')
                    return redirect('transfer')
                else:
                    url = 'https://fcsapi.com/api-v2/forex/converter?symbol=USD/EUR&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                    payload = {}
                    headers = {
                    'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                    }

                    response = requests.request("POST", url, headers=headers, data = payload)

                    #print(response.text.encode('utf8'))
                    r_dict = json.loads(response.text)
        
                    e = []
                    for i in r_dict:
                        dt = r_dict[i]
                        txn = e.append(dt)
                    trans = e[3]
                    total = trans['total']
                    t_amt = float(total)
                    new = rb + t_amt
                    new1 = (str(round(new, 1)))
                    Account.objects.filter(phone_no=r_number).update(eur_bal=new1)

                    new_2 = (sb - (amt + c_amt))
                    Account.objects.filter(username=s_username).update(usd_bal=new_2)

                    trans = Transactions(sender=s_username,
                                        receiver=show,
                                        amount=new1,
                                        description=f'FT/EUR/{r_first_name}',
                                        ref_no=ref_no,
                                        cur='EUR',
                                        charges=c_amt)
                    trans.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, r_mail
                    html_content = render_to_string('mail/s_mail.html',
                                            {'first_name': first_name, 'new_2': new_2, 'ref_no': ref_no, 'amount': amt,
                                             'receiver': r_number, 'c_amt': c_amt, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #         Receiver Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, m_email
                    html_content = render_to_string('mail/r_mail.html',
                                            {'r_first_name': r_first_name, 'new': new, 'ref_no': ref_no, 'amount': amt,
                                             'sender': first_name, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return render(request, 'send-money-success.html', {'amount': amt, 'r_first_name': r_first_name})
            elif s_cur == "USD" and r_cur == "GBP":
                f_charge = (float(charge))
                sb = (float(s_bal.usd_bal))
                rb = (float(r_bal.gbp_bal))
                amt = (float(amount))
                c_amt = amt * (f_charge / 100)
                if amt > sb:
                    messages.warning(request, 'Insufficient Balance')
                    return redirect('transfer')
                else:
                    url = 'https://fcsapi.com/api-v2/forex/converter?symbol=USD/GBP&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                    payload = {}
                    headers = {
                    'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                    }

                    response = requests.request("POST", url, headers=headers, data = payload)

                    #print(response.text.encode('utf8'))
                    r_dict = json.loads(response.text)
        
                    e = []
                    for i in r_dict:
                        dt = r_dict[i]
                        txn = e.append(dt)
                    trans = e[3]
                    total = trans['total']
                    t_amt = float(total)
                    new = rb + t_amt
                    new1 = (str(round(new, 1)))
                    Account.objects.filter(phone_no=r_number).update(gbp_bal=new1)

                    new_2 = (sb - (amt + c_amt))
                    Account.objects.filter(username=s_username).update(usd_bal=new_2)

                    trans = Transactions(sender=s_username,
                                        receiver=show,
                                        amount=new1,
                                        description='FT/GBP/{r_first_name}',
                                        ref_no=ref_no,
                                        cur='GBP',
                                        charges=c_amt)
                    trans.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, r_mail
                    html_content = render_to_string('mail/s_mail.html',
                                            {'first_name': first_name, 'new_2': new_2, 'ref_no': ref_no, 'amount': amt,
                                             'receiver': r_number, 'c_amt': c_amt, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #         Receiver Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, m_email
                    html_content = render_to_string('mail/r_mail.html',
                                            {'r_first_name': r_first_name, 'new': new, 'ref_no': ref_no, 'amount': amt,
                                             'sender': first_name, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return render(request, 'send-money-success.html', {'amount': amt, 'r_first_name': r_first_name})
            elif s_cur == "EUR" and r_cur == "NGN":
                f_charge = (float(charge))
                sb = (float(s_bal.eur_bal))
                rb = (float(r_bal.bal))
                amt = (float(amount))
                c_amt = amt * (f_charge / 100)
                if amt > sb:
                    messages.warning(request, 'Insufficient Balance')
                    return redirect('transfer')
                else:
                    url = 'https://fcsapi.com/api-v2/forex/converter?symbol=EUR/NGN&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                    payload = {}
                    headers = {
                    'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                    }

                    response = requests.request("POST", url, headers=headers, data = payload)

                    #print(response.text.encode('utf8'))
                    r_dict = json.loads(response.text)
        
                    e = []
                    for i in r_dict:
                        dt = r_dict[i]
                        txn = e.append(dt)
                    trans = e[3]
                    total = trans['total']
                    t_amt = float(total)
                    new = rb + t_amt
                    new1 = (str(round(new, 1)))
                    Account.objects.filter(phone_no=r_number).update(bal=new1)

                    new_2 = (sb - (amt + c_amt))
                    Account.objects.filter(username=s_username).update(eur_bal=new_2)

                    trans = Transactions(sender=s_username,
                                        receiver=show,
                                        amount=new1,
                                        description=f'FT/NGN/{r_first_name}',
                                        ref_no=ref_no,
                                        cur='NGN',
                                        charges=c_amt)
                    trans.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, r_mail
                    html_content = render_to_string('mail/s_mail.html',
                                            {'first_name': first_name, 'new_2': new_2, 'ref_no': ref_no, 'amount': amt,
                                             'receiver': r_number, 'c_amt': c_amt, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #         Receiver Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, m_email
                    html_content = render_to_string('mail/r_mail.html',
                                            {'r_first_name': r_first_name, 'new': new, 'ref_no': ref_no, 'amount': amt,
                                             'sender': first_name, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return render(request, 'send-money-success.html', {'amount': amt, 'r_first_name': r_first_name})
            elif s_cur == "EUR" and r_cur == "EUR":
                f_charge = (float(charge))
                sb = (float(s_bal.eur_bal))
                rb = (float(r_bal.eur_bal))
                am = (float(amount))
                c_amt = am * (f_charge / 100)
                if am > sb:
                    messages.warning(request, 'Insufficient Balance')
                    return redirect('transfer')
                else:
                    new = rb + am
                    Account.objects.filter(phone_no=r_number).update(eur_bal=new)

                    new_2 = (sb - (am + c_amt))
                    Account.objects.filter(username=s_username).update(eur_bal=new_2)

                    trans = Transactions(sender=s_username,
                                        receiver=show,
                                        amount=amount,
                                        description=f'FT/EUR/{r_first_name}',
                                        ref_no=ref_no,
                                        cur='EUR',
                                        charges=c_amt)
                    trans.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, r_mail
                    html_content = render_to_string('mail/s_mail.html',
                                            {'first_name': first_name, 'new_2': new_2, 'ref_no': ref_no, 'amount': am,
                                             'receiver': r_number, 'c_amt': c_amt, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #         Receiver Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, m_email
                    html_content = render_to_string('mail/r_mail.html',
                                            {'r_first_name': r_first_name, 'new': new, 'ref_no': ref_no, 'amount': am,
                                             'sender': first_name, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return render(request, 'send-money-success.html', {'amount': am, 'r_first_name': r_first_name})
            elif s_cur == "EUR" and r_cur == "USD":
                f_charge = (float(charge))
                sb = (float(s_bal.eur_bal))
                rb = (float(r_bal.usd_bal))
                amt = (float(amount))
                c_amt = amt * (f_charge / 100)
                if amt > sb:
                    messages.warning(request, 'Insufficient Balance')
                    return redirect('transfer')
                else:
                    url = 'https://fcsapi.com/api-v2/forex/converter?symbol=EUR/USD&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                    payload = {}
                    headers = {
                    'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                    }

                    response = requests.request("POST", url, headers=headers, data = payload)

                    #print(response.text.encode('utf8'))
                    r_dict = json.loads(response.text)
        
                    e = []
                    for i in r_dict:
                        dt = r_dict[i]
                        txn = e.append(dt)
                    trans = e[3]
                    total = trans['total']
                    t_amt = float(total)
                    new = rb + t_amt
                    new1 = (str(round(new, 1)))
                    Account.objects.filter(phone_no=r_number).update(usd_bal=new1)

                    new_2 = (sb - (amt + c_amt))
                    Account.objects.filter(username=s_username).update(eur_bal=new_2)

                    trans = Transactions(sender=s_username,
                                        receiver=show,
                                        amount=new1,
                                        description=f'FT/USD/{r_first_name}',
                                        ref_no=ref_no,
                                        cur='USD',
                                        charges=c_amt)
                    trans.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, r_mail
                    html_content = render_to_string('mail/s_mail.html',
                                            {'first_name': first_name, 'new_2': new_2, 'ref_no': ref_no, 'amount': amt,
                                             'receiver': r_number, 'c_amt': c_amt, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #         Receiver Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, m_email
                    html_content = render_to_string('mail/r_mail.html',
                                            {'r_first_name': r_first_name, 'new': new, 'ref_no': ref_no, 'amount': amt,
                                             'sender': first_name, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return render(request, 'send-money-success.html', {'amount': amt, 'r_first_name': r_first_name})
            elif s_cur == "EUR" and r_cur == "GBP":
                f_charge = (float(charge))
                sb = (float(s_bal.eur_bal))
                rb = (float(r_bal.gbp_bal))
                amt = (float(amount))
                c_amt = amt * (f_charge / 100)
                if amt > sb:
                    messages.warning(request, 'Insufficient Balance')
                    return redirect('transfer')
                else:
                    url = 'https://fcsapi.com/api-v2/forex/converter?symbol=EUR/GBP&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                    payload = {}
                    headers = {
                    'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                    }

                    response = requests.request("POST", url, headers=headers, data = payload)

                    #print(response.text.encode('utf8'))
                    r_dict = json.loads(response.text)
        
                    e = []
                    for i in r_dict:
                        dt = r_dict[i]
                        txn = e.append(dt)
                    trans = e[3]
                    total = trans['total']
                    t_amt = float(total)
                    new = rb + t_amt
                    new1 = (str(round(new, 1)))
                    Account.objects.filter(phone_no=r_number).update(gbp_bal=new1)

                    new_2 = (sb - (amt + c_amt))
                    Account.objects.filter(username=s_username).update(eur_bal=new_2)

                    trans = Transactions(sender=s_username,
                                        receiver=show,
                                        amount=new1,
                                        description=f'FT/GBP/{r_first_name}',
                                        ref_no=ref_no,
                                        cur='GBP',
                                        charges=c_amt)
                    trans.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, r_mail
                    html_content = render_to_string('mail/s_mail.html',
                                            {'first_name': first_name, 'new_2': new_2, 'ref_no': ref_no, 'amount': amt,
                                             'receiver': r_number, 'c_amt': c_amt, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #         Receiver Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, m_email
                    html_content = render_to_string('mail/r_mail.html',
                                            {'r_first_name': r_first_name, 'new': new, 'ref_no': ref_no, 'amount': amt,
                                             'sender': first_name, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return render(request, 'send-money-success.html', {'amount': amt, 'r_first_name': r_first_name})
            elif s_cur == "GBP" and r_cur == "NGN":
                f_charge = (float(charge))
                sb = (float(s_bal.gbp_bal))
                rb = (float(r_bal.bal))
                amt = (float(amount))
                c_amt = amt * (f_charge / 100)
                if amt > sb:
                    messages.warning(request, 'Insufficient Balance')
                    return redirect('transfer')
                else:
                    url = 'https://fcsapi.com/api-v2/forex/converter?symbol=GBP/NGN&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                    payload = {}
                    headers = {
                    'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                    }

                    response = requests.request("POST", url, headers=headers, data = payload)

                    #print(response.text.encode('utf8'))
                    r_dict = json.loads(response.text)
        
                    e = []
                    for i in r_dict:
                        dt = r_dict[i]
                        txn = e.append(dt)
                    trans = e[3]
                    total = trans['total']
                    t_amt = float(total)
                    new = rb + t_amt
                    new1 = (str(round(new, 1)))
                    Account.objects.filter(phone_no=r_number).update(bal=new1)

                    new_2 = (sb - (amt + c_amt))
                    Account.objects.filter(username=s_username).update(gbp_bal=new_2)

                    trans = Transactions(sender=s_username,
                                        receiver=show,
                                        amount=new1,
                                        description=f'FT/NGN/{r_first_name}',
                                        ref_no=ref_no,
                                        cur='NGN',
                                        charges=c_amt)
                    trans.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, r_mail
                    html_content = render_to_string('mail/s_mail.html',
                                            {'first_name': first_name, 'new_2': new_2, 'ref_no': ref_no, 'amount': amt,
                                             'receiver': r_number, 'c_amt': c_amt, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #         Receiver Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, m_email
                    html_content = render_to_string('mail/r_mail.html',
                                            {'r_first_name': r_first_name, 'new': new, 'ref_no': ref_no, 'amount': amt,
                                             'sender': first_name, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return render(request, 'send-money-success.html', {'amount': amt, 'r_first_name': r_first_name})
            elif s_cur == "GBP" and r_cur == "GBP":
                f_charge = (float(charge))
                sb = (float(s_bal.gbp_bal))
                rb = (float(r_bal.gbp_bal))
                am = (float(amount))
                c_amt = am * (f_charge / 100)
                if am > sb:
                    messages.warning(request, 'Insufficient Balance')
                    return redirect('transfer')
                else:
                    new = rb + am
                    Account.objects.filter(phone_no=r_number).update(gbp_bal=new)

                    new_2 = (sb - (am + c_amt))
                    Account.objects.filter(username=s_username).update(gbp_bal=new_2)

                    trans = Transactions(sender=s_username,
                                        receiver=show,
                                        amount=amount,
                                        description=f'FT/GBP/{r_first_name}',
                                        ref_no=ref_no,
                                        cur='GBP',
                                        charges=c_amt)
                    trans.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, r_mail
                    html_content = render_to_string('mail/s_mail.html',
                                            {'first_name': first_name, 'new_2': new_2, 'ref_no': ref_no, 'amount': am,
                                             'receiver': r_number, 'c_amt': c_amt, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #         Receiver Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, m_email
                    html_content = render_to_string('mail/r_mail.html',
                                            {'r_first_name': r_first_name, 'new': new, 'ref_no': ref_no, 'amount': am,
                                             'sender': first_name, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return render(request, 'send-money-success.html', {'amount': am, 'r_first_name': r_first_name})
            elif s_cur == "GBP" and r_cur == "USD":
                f_charge = (float(charge))
                sb = (float(s_bal.gbp_bal))
                rb = (float(r_bal.usd_bal))
                amt = (float(amount))
                c_amt = amt * (f_charge / 100)
                if amt > sb:
                    messages.warning(request, 'Insufficient Balance')
                    return redirect('transfer')
                else:
                    url = 'https://fcsapi.com/api-v2/forex/converter?symbol=GBP/USD&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                    payload = {}
                    headers = {
                    'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                    }

                    response = requests.request("POST", url, headers=headers, data = payload)

                    #print(response.text.encode('utf8'))
                    r_dict = json.loads(response.text)
        
                    e = []
                    for i in r_dict:
                        dt = r_dict[i]
                        txn = e.append(dt)
                    trans = e[3]
                    total = trans['total']
                    t_amt = float(total)
                    new = rb + t_amt
                    new1 = (str(round(new, 1)))
                    Account.objects.filter(phone_no=r_number).update(usd_bal=new1)

                    new_2 = (sb - (amt + c_amt))
                    Account.objects.filter(username=s_username).update(gbp_bal=new_2)

                    trans = Transactions(sender=s_username,
                                        receiver=show,
                                        amount=new1,
                                        description=f'FT/USD/{r_first_name}',
                                        ref_no=ref_no,
                                        cur='USD',
                                        charges=c_amt)
                    trans.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, r_mail
                    html_content = render_to_string('mail/s_mail.html',
                                            {'first_name': first_name, 'new_2': new_2, 'ref_no': ref_no, 'amount': amt,
                                             'receiver': r_number, 'c_amt': c_amt, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #         Receiver Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, m_email
                    html_content = render_to_string('mail/r_mail.html',
                                            {'r_first_name': r_first_name, 'new': new, 'ref_no': ref_no, 'amount': amt,
                                             'sender': first_name, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return render(request, 'send-money-success.html', {'amount': amt, 'r_first_name': r_first_name})
            elif s_cur == "GBP" and r_cur == "EUR":
                f_charge = (float(charge))
                sb = (float(s_bal.gbp_bal))
                rb = (float(r_bal.eur_bal))
                amt = (float(amount))
                c_amt = amt * (f_charge / 100)
                if amt > sb:
                    messages.warning(request, 'Insufficient Balance')
                    return redirect('transfer')
                else:
                    url = 'https://fcsapi.com/api-v2/forex/converter?symbol=GBP/EUR&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                    payload = {}
                    headers = {
                    'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                    }

                    response = requests.request("POST", url, headers=headers, data = payload)

                    #print(response.text.encode('utf8'))
                    r_dict = json.loads(response.text)
        
                    e = []
                    for i in r_dict:
                        dt = r_dict[i]
                        txn = e.append(dt)
                    trans = e[3]
                    total = trans['total']
                    t_amt = float(total)
                    new = rb + t_amt
                    new1 = (str(round(new, 1)))
                    Account.objects.filter(phone_no=r_number).update(eur_bal=new1)

                    new_2 = (sb - (amt + c_amt))
                    Account.objects.filter(username=s_username).update(gbp_bal=new_2)

                    trans = Transactions(sender=s_username,
                                        receiver=show,
                                        amount=new1,
                                        description=f'FT/NGN/{r_first_name}',
                                        ref_no=ref_no,
                                        cur='EUR',
                                        charges=c_amt)
                    trans.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, r_mail
                    html_content = render_to_string('mail/s_mail.html',
                                            {'first_name': first_name, 'new_2': new_2, 'ref_no': ref_no, 'amount': amt,
                                             'receiver': r_number, 'c_amt': c_amt, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #         Receiver Email Notification
                    subject, from_email, to = 'Fund Transfer', EMAIL_FROM, m_email
                    html_content = render_to_string('mail/r_mail.html',
                                            {'r_first_name': r_first_name, 'new': new, 'ref_no': ref_no, 'amount': amt,
                                             'sender': first_name, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return render(request, 'send-money-success.html', {'amount': amt, 'r_first_name': r_first_name})          
    all_bal = Account.objects.all().get(username=c_user)
    return render(request, 'transfer.html', {'all_bal': all_bal})


# Verify function the verify the user of the wallet
@login_required(login_url='login')
def verify(request):
    if request.method == 'POST':
        r_number = request.POST['number']
        r_cur = request.POST['cur']
        s_cur = request.POST['s_cur']
        num = Account.objects.filter(phone_no=r_number).exists()
        if num:
            check = Account.objects.all().get(phone_no=r_number)
            charge = Commission.objects.values('transfer').get(id=1)['transfer']
            cont = {'check': check, 'num': num, 'charge': charge, 'r_cur': r_cur, 's_cur': s_cur}
            return render(request, 'transfer.html', cont)
        else:
            messages.error(request, 'Mobile Number Not Found')
            return redirect('transfer')

    return render(request, 'transfer.html')


@login_required(login_url='login')
def profile(request):
    c_user = request.user.username
    show = Account.objects.values('phone_no').get(username=c_user)['phone_no']
    bal = Account.objects.all().get(username=c_user)
    cus_id = Account.objects.values('customer_id').get(username=c_user)['customer_id']
    status = Account.objects.values('status').get(username=c_user)['status']
    return render(request, 'profile.html', {'show': show, 'cus_id': cus_id,
                                            'status': status, 'bal': bal})


# Setting view function
@login_required(login_url='login')
def merchant(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    c_user = request.user.username
    if Merchant.objects.filter(bus_owner_username=c_user).exists():
        s_show = Merchant.objects.all().get(bus_owner_username=c_user)
        show = Account.objects.values('phone_no').get(username=c_user)['phone_no']
        i_mode = Merchant.objects.values('int_mode').get(bus_owner_username=c_user)['int_mode']
        w_p_charges = Merchant.objects.values('w_p_charges').get(bus_owner_username=c_user)['w_p_charges']
        cus_id = Account.objects.values('customer_id').get(username=c_user)['customer_id']
        vendor = Transactions.objects.filter(receiver=c_user).filter(
            description='Merchant').aggregate(Sum('amount'))['amount__sum']
        context = {'s_show': s_show, 'show': show, 'cus_id': cus_id,
                   'i_mode': i_mode, 'w_p_charges': w_p_charges, 'vendor': vendor}
        return render(request, 'merchant.html', context)
    else:
        return redirect('api')


# The view that handle all the transaction activity
@login_required(login_url='login')
def activity(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    c_user = request.user.username
    show = Transactions.objects.filter(Q(sender=c_user) | Q(receiver=c_user)).order_by('-id')
    vendor = Transactions.objects.filter(receiver=c_user).filter(
        description='Merchant').aggregate(Sum('amount'))['amount__sum']
    shows = Account.objects.all().get(username=c_user)
    # print("Hello:", show)
    paginator = Paginator(show, 5)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    context = {'show': page_obj, 'vendor': vendor, 'shows': shows}
    return render(request, 'activity.html', context)


# Voucher View function that handle the voucher generation
@login_required(login_url='login')
def voucher(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    N = 10
    code = ''.join(random.choices(string.digits, k=N))
    ref_no = uuid.uuid4().hex[:10].upper()
    c_user = request.user.username
    if request.method == 'POST':
        # v_username = request.POST['v_username']
        amount = request.POST['amount']
        s_bal = Account.objects.values('bal').get(username=c_user)['bal']
        status = Account.objects.values('status').get(username=c_user)['status']
        sb = (float(s_bal))
        am = (float(amount))

        if status == "hold":
            messages.warning(request, 'Your Account is Currently on Hold ')
            return render(request, 'errors.html')
        elif am > sb:
            messages.error(request, 'Insufficient Balance')
            return render(request, 'errors.html')
        else:
            new_2 = sb - am
            Account.objects.filter(username=c_user).update(bal=new_2)
            v_save = Voucher(v_creator=c_user,
                             v_code=code,
                             v_amount=am,
                             ref_no=ref_no,
                             v_status='open',
                             )
            trans = Transactions(sender=c_user,
                                 receiver='voucher',
                                 amount=amount,
                                 description='Voucher',
                                 cur="NGN",
                                 ref_no=ref_no,
                                 charges=0)
            trans.save()
            v_save.save()
            messages.success(request, "Transaction Successful!!")
            return render(request, 'success.html')
    return render(request, 'voucher.html')


@login_required(login_url='login')
def all_voucher(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    c_user = request.user.username
    show = Voucher.objects.filter(Q(v_creator=c_user)).order_by('-id')
    vendor = Transactions.objects.filter(receiver=c_user).filter(
        description='Merchant').aggregate(Sum('amount'))['amount__sum']
    shows = Account.objects.all().get(username=c_user)
    paginator = Paginator(show, 5)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    context = {'show': page_obj, 'shows': shows, 'vendor': vendor}
    return render(request, 'all-voucher.html', context)


# View function that handle voucher redeeming
@login_required(login_url='login')
def load_voucher(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    ref_no = uuid.uuid4().hex[:10].upper()
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    c_user = request.user.username
    if request.method == 'POST':
        v_code = request.POST['v_code']
        l_username = request.POST['l_username']
        if Voucher.objects.filter(v_code=v_code).exists():
            v_am = Voucher.objects.values('v_amount').get(v_code=v_code)['v_amount']
            status = Voucher.objects.values('v_status').get(v_code=v_code)['v_status']
            loader = Voucher.objects.values('v_creator').get(v_code=v_code)['v_creator']
            bal = Account.objects.values('bal').get(username=c_user)['bal']
            a_status = Account.objects.values('status').get(username=c_user)['status']
            sb = (float(bal))
            am = (float(v_am))

            if a_status == "hold":
                messages.warning(request, "Your Account is Currently on Hold")
                return render(request, 'errors.html')
            if status == 'close':
                messages.error(request, "Voucher Used")
                return render(request, 'errors.html')
            elif loader == l_username:
                messages.warning(request, "You Cant load the Voucher you Created")
                return render(request, 'errors.html')
            else:
                new = sb + am
                Account.objects.filter(username=l_username).update(bal=new)
                Voucher.objects.filter(v_code=v_code).update(v_status='close', v_loader=c_user, v_date_load=now)
                messages.success(request, "Transaction Successful!!")

                trans = Transactions(sender='Voucher', receiver=l_username, amount=am,
                                     description='Voucher', ref_no=ref_no, charges=0)
                trans.save()
                return render(request, 'success.html')
        else:
            messages.info(request, "Invalid Code/Code Doesn't Exist!!")
            return redirect('load_voucher')
    show = Voucher.objects.filter(Q(v_creator=c_user))
    context = {'show': show}
    return render(request, 'load_voucher.html', context)


@login_required(login_url='login')
def ticket(request):
    N = 6
    ticket_id = ''.join(random.choices(string.digits, k=N))
    c_user = request.user.username
    if request.method == 'POST':
        subject = request.POST['subject']
        category = request.POST['category']
        content = request.POST['content']
        priority = request.POST['priority']

        t_save = Ticket(subject=subject,
                        category=category,
                        owner=c_user,
                        content=content,
                        priority=priority,
                        status='open',
                        ticket_id=ticket_id,)
        t_save.save()
        messages.info(request, "Ticket Created")

    shows = Ticket.objects.filter(Q(owner=c_user)).order_by('-id')
    show = Account.objects.all().get(username=c_user)
    vendor = Transactions.objects.filter(receiver=c_user).filter(
        description='Merchant').aggregate(Sum('amount'))['amount__sum']
    paginator = Paginator(shows, 5)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    context = {'show': show, 'shows': page_obj, 'vendor': vendor}
    return render(request, 'ticket.html', context)


def compose(request):
    return render(request, 'compose.html')


@login_required(login_url='login')
def dispute(request):
    c_user = request.user.username
    show = Ticket.objects.filter(Q(owner=c_user))
    context = {'show': show}
    return render(request, 'dispute.html', context)


@login_required(login_url='login')
def merchant_i(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    c_user = request.user.username
    if request.method == 'POST':
        b_name = request.POST['b_name']
        b_address = request.POST['b_address']
        b_email = request.POST['b_email']
        b_tel = request.POST['b_tel']
        b_url = request.POST['b_url']
        bus_logo = request.FILES['bus_logo']

        if Account.objects.values('status').get(username=c_user)['status'] == "hold":
            messages.info(request, "You Account is Currently o Hold")
            return redirect('merchant')
        elif Merchant.objects.filter(bus_owner_username=c_user).exists():
            messages.warning(request, "You are a Merchant Already")
            return redirect('merchant')
        else:
            b_save = Merchant(bus_owner_username=c_user, bus_name=b_name, bus_address=b_address, bus_email=b_email,
                              bus_no=b_tel, bus_website=b_url, bus_logo=bus_logo)
            b_save.save()
            messages.success(request, "Merchant Registration Successful")
            return redirect('merchant')
    show = Account.objects.values('phone_no').get(username=c_user)['phone_no']
    context = {'show': show}
    return render(request, 'merchant.html', context)


@login_required(login_url='login')
def api(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    c_user = request.user.username
    api_test = uuid.uuid4().hex[:50].lower()
    api_live = uuid.uuid4().hex[:50].lower()
    if request.method == 'POST':
        user = request.POST['user']
        if Account.objects.values('status').get(username=c_user)['status'] == "hold":
            messages.error(request, "You Account is Currently o Hold")
            return redirect('merchant')
        elif Merchant.objects.filter(bus_owner_username=c_user).exists():
            Merchant.objects.filter(bus_owner_username=user).update(api_test_key=api_test, api_live_key=api_live)
            messages.success(request, "Please Find Your API key on API Tab")
            return redirect('merchant')
        else:
            messages.warning(request, "You have to be a Merchant before you can use our API")
            return redirect('merchant')
    show = Account.objects.values('phone_no').get(username=c_user)['phone_no']
    cus_id = Account.objects.values('customer_id').get(username=c_user)['customer_id']
    context = {'show': show, 'cus_id': cus_id}
    return render(request, 'merchant.html', context)


@login_required(login_url='login')
def reply(request, ticket_id):
    show = Ticket.objects.filter(Q(ticket_id=ticket_id))
    r_show = Resolution.objects.filter(Q(ticket_id=ticket_id))
    get_content = Ticket.objects.all().get(ticket_id=ticket_id)
    context = {'show': show, 'r_show': r_show, 'get_content': get_content}
    return render(request, 'ticket.html', context)


@login_required(login_url='login')
def resolution(request):
    if request.method == 'POST':
        ticket_id = request.POST['ticket_id']
        subject = request.POST['subject']
        category = request.POST['category']
        content = request.POST['content']

        t_save = Resolution(ticket_id=ticket_id, subject=subject, category=category, content=content)
        t_save.save()
        # messages.info(request, "Reply Successful!!")
        return redirect('ticket')
    return render(request, 'ticket.html')


@login_required(login_url='login')
def bank(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    c_user = request.user.username
    if request.method == 'POST':
        acct_name = request.POST['acct_name']
        acct_no = request.POST['acct_no']
        bank_name = request.POST['bank_name']
        code = Banks.objects.values('code').get(name=bank_name)['code']
        status = Account.objects.values('status').get(username=c_user)['status']
        if status == "hold":
            messages.warning(request, "You Account is Currently o Hold")
            return redirect('bank')
        else:
            s_bank = Bank(username=c_user, account_name=acct_name, account_no=acct_no, bank_name=bank_name, code=code)
            s_bank.save()
            messages.success(request, "Added Successful!!")
            return redirect('bank')
    all_bank = Banks.objects.filter()
    shows = Account.objects.all().get(username=c_user)
    if Bank.objects.filter(username=c_user).exists():
        show = Bank.objects.all().get(username=c_user)
        ch = Bank.objects.filter(username=c_user).exists()
        context = {'show': show, 'bank': all_bank, 'shows': shows, 'ch': ch}
        return render(request, 'add_bank_acc.html', context)
    else:
        context = {'bank': all_bank, 'shows': shows}
        return render(request, 'add_bank_acc.html', context)


@login_required(login_url='login')
def delete(request, id):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    Bank.objects.filter(id=id).delete()
    messages.info(request, 'Account Deleted')
    return render(request, 'msg-success.html')


@login_required(login_url='login')
def withdraw(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    c_user = request.user.username
    if request.method == "POST":
        acct_no = request.POST['acct_no']
        amount = request.POST['amount']
        bank = Bank.objects.values('code').get(username=c_user)['code']
        url = f"https://sandbox.monnify.com/api/v1/disbursements/account/validate?accountNumber={acct_no}&bankCode={bank}"

        payload = {}
        headers= {}

        response = requests.request("GET", url, headers=headers, data = payload)

        r_dict = json.loads(response.text)

        acc = []
        for i in r_dict:
            bnk = r_dict[i]
            acc.append(bnk)
        status = acc[1]
        #print(acc)
        if status == "success":
            ac_d = acc[3]
            acct_num = ac_d['accountNumber']
            acct_name = ac_d['accountName']
            bnk_code = ac_d['bankCode']
            bank_name = Bank.objects.all().get(username=c_user)
            return render(request, 'withdraw-money-confirm.html', {'acct_num': acct_no,
                                                               'amount': amount,
                                                               'acct_name': acct_name,
                                                               'bnk_code': bnk_code,
                                                               'bank_name': bank_name.bank_name})
    shows = Account.objects.all().get(username=c_user)
    show = Bank.objects.filter(Q(username=c_user))
    context = {'show': show, 'shows': shows}
    return render(request, 'withdraw.html', context)


@login_required(login_url='login')
def initiate_withdraw(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    ref_no = uuid.uuid4().hex[:10].upper()
    c_user = request.user.username
    if request.method == 'POST':
        acct_no = request.POST['acct_no']
        amount = request.POST['amount']
        bnk_code = request.POST['bnk_code']
        acct_name = Bank.objects.values('account_name').get(username=c_user)['account_name']
        bank_name = Bank.objects.values('bank_name').get(username=c_user)['bank_name']
        bal = Account.objects.values('bal').get(username=c_user)['bal']
        status = Account.objects.values('status').get(username=c_user)['status']
        first_name = User.objects.values('first_name').get(username=c_user)['first_name']
        email = User.objects.values('email').get(username=c_user)['email']
        sb = (float(bal))
        am = (float(amount))

        if status == "hold":
            messages.error(request, 'Sorry, You Account is on Hold')
            return redirect('withdraw')
        elif am <= 0:
            messages.warning(request, 'Invalid Transaction')
            return redirect('withdraw')
        elif sb < am:
            messages.warning(request, 'Low Balance ')
            return redirect('withdraw')
        elif am < 1000.0:
            messages.warning(request, 'Withdraw Minimum is 1,000 ')
            return redirect('withdraw')
        else:
            new = sb - am

            url = "https://sandbox.monnify.com/api/v1/disbursements/single"

            payload = '{\n    \"amount\": '+ amount +',\n    \"reference\":\"'+ ref_no +'\",\n    \"narration\":\"Withdraw\",\n    \"bankCode\": \"' + bnk_code +'\",\n    \"accountNumber\": \"'+ acct_no +'\",\n    \"currency\": \"NGN\",\n    \"walletId\": \"654CAB2118124760A659C787B2AA38E8\"\n}'
            headers = {
            'Authorization': 'Basic TUtfVEVTVF84VUJYR0tURlNCOkVOUkM0RkRZS1NUVVlRS0E1M1lQWEJGTFVGWFdZSEcy',
            'Content-Type': 'application/json'
            }

            response = requests.request("POST", url, headers=headers, data = payload)

            r_dict = json.loads(response.text)

            resp = []
            for i in r_dict:
                bnk = r_dict[i]
                resp.append(bnk)
            res = resp[3]
            amount = res['amount']
            ref = res['reference']
            status = res['status']
            Account.objects.filter(username=c_user).update(bal=new)
            s_withdraw = Withdraw(username=c_user, amount=amount, acct_name=acct_name, acct_no=acct_no,
                                  bank_name=bank_name, status=status, ref_no=ref, )
            trans = Transactions(sender=c_user,
                                 receiver='Withdrawal' + '/' + '/'+ acct_no + acct_name,
                                 amount=amount,
                                 description='Debit',
                                 ref_no=ref, cur="NGN", charges=0)
            trans.save()
            s_withdraw.save()
            subject, from_email, to = 'Fund Withdrawal', EMAIL_FROM, email
            html_content = render_to_string('mail/withdraw.html',
                                            {'bank_name': bank_name, 'amount': amount,
                                             'new': new, 'ref_no': ref_no, 'date': now,
                                             'first_name': first_name, 'acct_name': acct_name, 'acct_no': acct_no})
            text_content = strip_tags(html_content)
            msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
            msg.attach_alternative(html_content, "text/html")
            msg.send()
            return render(request, 'withdraw-money-success.html', {'amount': amount})


@login_required(login_url='login')
def withdrawal_history(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    c_user = request.user.username
    show = Withdraw.objects.filter(Q(username=c_user)).order_by('-id')
    shows = Account.objects.all().get(username=c_user)
    vendor = Transactions.objects.filter(receiver=c_user).filter(
        description='Merchant').aggregate(Sum('amount'))['amount__sum']
    paginator = Paginator(show, 5)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    return render(request, 'withdrawal-history.html', {'show': page_obj,
                                                       'shows': shows,
                                                       'vendor': vendor})


@login_required(login_url='login')
def deposit(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    ref_no = uuid.uuid4().hex[:10].upper()
    if request.method == 'POST':
        username = request.POST['username']
        email = request.POST['email']
        amount = request.POST['amount']
        show = Account.objects.values().get(username=username)['phone_no']
        request.session['ref_no'] = ref_no
        request.session['username'] = username
        request.session['amount'] = amount
        context = {'username': username, 'email': email, 'amount': amount, 'show': show, 'ref_no': ref_no}
        return render(request, 'confirm.html', context)
    else:
        return render(request, 'deposit.html')


def airtime_verify(request, reference):
    return render(request, 'airtime.html')


@login_required(login_url='login')
def airtime(request):
    ref_no = uuid.uuid4().hex[:10].upper()
    if request.method == 'POST':
        username = request.POST['username']
        email = request.POST['email']
        amount = request.POST['amount']
        show = Account.objects.values().get(username=username)['phone_no']

        request.session['ref_no'] = ref_no
        request.session['username'] = username
        request.session['amount'] = amount
        context = {'username': username, 'email': email, 'amount': amount, 'show': show, 'ref_no': ref_no}
        return render(request, 'airtime_confirm.html', context)
    else:
        return render(request, 'airtime.html')


def airtime_confirm(request):
    return render(request, 'airtime_confirm.html')


@login_required(login_url='login')
def confirm(request):
    return render(request, 'confirm.html')


# @login_required(login_url='login')
# def payment(request):
#     username = request.session['username']
#     amount = request.session['amount']
#     ref_no = request.session['ref_no']
#     bal = Account.objects.values('bal').get(username=username)['bal']
#     sb = (float(bal))
#     am = (float(amount))
#     new = sb + am
#     Account.objects.filter(username=username).update(bal=new)
#     messages.info(request, 'Transaction Successful')
#     trans = Transactions(sender='Card Payment', receiver=username, amount=am, description='Deposit', ref_no=ref_no, )
#     trans.save()
#     del request.session['username']
#     del request.session['amount']
#     del request.session['ref_no']
#     return render(request, 'deposit.html')


@login_required(login_url='login')
def invoice_verify(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    if request.method == 'POST':
        phone = request.POST['phone']
        check_user = Account.objects.filter(phone_no=phone).exists()
        if check_user:
            check = Account.objects.all().get(phone_no=phone)
            cont = {'check': check, 'check_user': check_user}
            return render(request, 'invoice.html', cont)
        else:
            messages.error(request, 'User Not Found')
            return redirect('invoice')


@login_required(login_url='login')
def invoice(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    c_user = request.user.username
    ref_no = uuid.uuid4().hex[:10].upper()
    if request.method == 'POST':
        s_username = request.POST['s_username']
        r_phone = request.POST['phone']
        amount = request.POST['amount']
        content = request.POST['content']
        date = request.POST['date']
        time = request.POST['time']
        status = Account.objects.values('status').get(username=c_user)['status']
        r_status = Account.objects.values('status').get(phone_no=r_phone)['status']
        r_email = User.objects.values('email').get(phone=r_phone)['email']
        s_email = User.objects.values('email').get(username=s_username)['email']
        s_name = UserModel.objects.all().get(username=c_user)
        r_username = Account.objects.values('username').get(phone_no=r_phone)['username']
        u_time = time + ":00"
        final_date = date + " " + u_time
        cname = Account.objects.all().get(phone_no=r_phone)

        if status == "hold":
            messages.warning(request, 'Your Account is on hold. Kindly contact our Agent')
            return redirect('invoice')
        elif r_status == "hold":
            messages.warning(request, "This Customer Can't receive Money at the Moment")
            return redirect('invoice')
        elif c_user == r_username:
            messages.error(request, "Please You Can't Send Invoice to Yourself")
            return redirect('invoice')
        else:
            url = "https://sandbox.monnify.com/api/v1/invoice/create"

            payload = '{\n    \"amount\": \"'+ amount +'\",\n    \"invoiceReference\": \"'+ ref_no +'\",\n    \"description\": \"'+ content +'\",\n    \"currencyCode\": \"NGN\",\n    \"contractCode\": \"2917634474\",\n    \"customerEmail\": \"'+ r_email +'\",\n    \"customerName\": \"'+ cname.first_name + ' ' + cname.last_name +'\",\n    \"expiryDate\": \"'+ final_date +'\",\n    \"paymentMethods\": [\"ACCOUNT_TRANSFER\"],\n    \"redirectUrl\": \"http://' + request.get_host() +'/account/invoice_success\"\n}'
            headers = {
            'Authorization': 'Basic TUtfVEVTVF84VUJYR0tURlNCOkVOUkM0RkRZS1NUVVlRS0E1M1lQWEJGTFVGWFdZSEcy',
            'Content-Type': 'application/json'
            }

            response = requests.request("POST", url, headers=headers, data = payload)

            r_dict = json.loads(response.text)

            resp = []
            for i in r_dict:
                bnk = r_dict[i]
                resp.append(bnk)
            res = resp[3]
            if resp[0] == True:
                invoiceRef = res['invoiceReference']
                status = res['invoiceStatus']
                checkoutUrl = res['checkoutUrl']
                acct_no = res['accountNumber']
                acct_name = res['accountName']
                bank_name = res['bankName']
                bank_code = res['bankCode']
                s_invoice = Invoice(sender=s_username, receiver=r_username, amount=amount, content=content, 
                acct_name=acct_name, acct_no=acct_no, bank_name=bank_name, bank_code=bank_code, 
                cust_name=cname.first_name + ' ' + cname.last_name, due_date=final_date, checkout_url=checkoutUrl, 
                inv_ref=invoiceRef, status=status, sender_name=s_name.first_name + ' ' + s_name.last_name, action='Pay Now')
                s_invoice.save()
                # Sender
                subject, from_email, to = 'Invoice', EMAIL_FROM, s_email
                html_content = render_to_string('mail/s_invoice.html',
                                        {'r_username': r_username, 's_username': s_username,
                                         'amount': amount, 'date': now})
                text_content = strip_tags(html_content)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
                #     Receiver
                subject, from_email, to = 'Invoice', EMAIL_FROM, r_email
                html_content = render_to_string('mail/r_invoice.html',
                                        {'r_username': r_username, 's_username': s_username,
                                         'amount': amount, 'content': content, 'date': now})
                text_content = strip_tags(html_content)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
                return render(request, 'invoice-success.html', {'amount': amount, 'r_username': r_username})
            else:
                messages.error(request, "Error!! Process Decline")
                return redirect('invoice')
    else:
        return render(request, 'invoice.html')


@login_required(login_url='login')
def all_invoice(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    c_user = request.user.username
    show = Invoice.objects.filter(Q(sender=c_user)).order_by('-id')
    vendor = Transactions.objects.filter(receiver=c_user).filter(
        description='Merchant').aggregate(Sum('amount'))['amount__sum']
    shows = Account.objects.all().get(username=c_user)
    paginator = Paginator(show, 5)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    context = {'show': page_obj, 'vendor': vendor, 'shows': shows}
    return render(request, 'all-invoice.html', context)


@login_required(login_url='login')
def pay_invoice(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    c_user = request.user.username
    shows = Account.objects.all().get(username=c_user)
    vendor = Transactions.objects.filter(receiver=c_user).filter(
        description='Merchant').aggregate(Sum('amount'))['amount__sum']
    show = Invoice.objects.filter(Q(receiver=c_user)).order_by('-id')
    paginator = Paginator(show, 5)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    context = {'show': page_obj, 'shows': shows, 'vendor': vendor}
    return render(request, 'pay_invoice.html', context)


@csrf_exempt
def success(request, id):
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))

    if request.method == "GET":
        resp = request.GET.get('paymentReference')
        print(resp)
        url = f"https://sandbox.monnify.com/api/v1/merchant/transactions/query?paymentReference={resp}"

        payload = {}
        headers = {
        'Authorization': 'Basic TUtfVEVTVF84VUJYR0tURlNCOkVOUkM0RkRZS1NUVVlRS0E1M1lQWEJGTFVGWFdZSEcy'
        }

        response = requests.request("GET", url, headers=headers, data = payload)

        r_dict = json.loads(response.text)
        
        e = []
        for i in r_dict:
            dt = r_dict[i]
            txn = e.append(dt)
        trans = e[3]
        #print(trans)
        paymentRef = trans['paymentReference']
        p_method = trans['paymentMethod']
        amount = trans['amount']
        transRef = trans['transactionReference']
        rec_email = trans['customerEmail']
        p_status = trans['paymentStatus']
        p_desc = trans['paymentDescription']
        customerName = trans['customerName']
        s_username = Invoice.objects.values('sender').get(inv_ref=paymentRef)['sender']
        s_email = User.objects.values('email').get(username=s_username)['email']
        user_bal = Account.objects.values('bal').get(username=s_username)['bal']

        new = (float(user_bal) + float(amount))

        Account.objects.filter(user_id=user_id).update(bal=new)
        Invoice.objects.filter(inv_ref=paymentRef).update(status=p_status, date_paid=now)

        create_trans_rec = Transactions(sender=s_username, ref_no=paymentRef, payment_date=now, 
        description=p_desc + "INVOICE Paid by " + customerName, cur="NGN", charge="0")
        create_trans_rec.save()

        subject, from_email, to = 'Invoice', EMAIL_FROM, s_email
        html_content = render_to_string('mail/s_mail_invoice.html',
                                        {'r_username': r_username, 's_username': s_username,
                                         'amount': amount, 'ref_no': ref_no, 'new2': new2, 'date': now})
        text_content = strip_tags(html_content)
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        return render(request, 'pay-invoice-success.html', {'s_username': s_username, 'amount': amount})


@login_required(login_url='login')
def reject(request, id):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    c_user = request.user.username
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    if Account.objects.values('status').get(username=c_user)['status'] == "hold":
        messages.error(request, 'Your Account is on hold')
        return render(request, 'pay-invoice-fail.html')
    elif Invoice.objects.values('status').get(id=id)['status'] == 'reject':
        messages.error(request, 'Invoice Already Rejected')
        return render(request, 'pay-invoice-fail.html')
    elif Invoice.objects.values('status').get(id=id)['status'] == 'paid':
        messages.warning(request, 'Invoice Already Paid')
        return render(request, 'pay-invoice-fail.html')
    else:
        Invoice.objects.filter(id=id).update(status='reject', date_paid=now)
        messages.success(request, 'Invoice Rejected')
        return render(request, 'pay-invoice-fail.html')


@login_required(login_url='login')
def paid_invoice(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    c_user = request.user.username
    shows = Account.objects.all().get(username=c_user)
    vendor = Transactions.objects.filter(receiver=c_user).filter(
        description='Merchant').aggregate(Sum('amount'))['amount__sum']
    show = Invoice.objects.filter(Q(sender=c_user))
    paginator = Paginator(show, 5)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    context = {'show': page_obj, 'shows': shows, 'vendor': vendor}
    return render(request, 'paid_invoice.html', context)


@login_required(login_url='login')
def load(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    if request.method == "POST":
        amount = request.POST['amount']
        p_mode = request.POST['p_mode']
        if p_mode == "paystack":
            return render(request, 'deposit-money-confirm.html', {'amount': amount,
                                                                  'p_mode': p_mode})
    return render(request, 'load.html')


@login_required(login_url='login')
def deposit(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    ref_no = uuid.uuid4().hex[:10].upper()
    if request.method == 'POST':
        p_mode = request.POST['p_mode']
        username = request.POST['username']
        email = request.POST['email']
        amount = request.POST['amount']
        show = Account.objects.values('phone_no').get(username=username)['phone_no']
        status = Account.objects.values('status').get(username=username)['status']
        api_pk = Settings.objects.values('paystack_api').get(id=1)['paystack_api']
        charge = Commission.objects.values('deposit').get(id=1)['deposit']
        f_charge = (float(charge))
        am = (float(amount))
        c_amt = am * (f_charge / 100)
        t_amt = am + c_amt
        if p_mode == "paystack":
            if status == "hold":
                messages.error(request, "You Account is Currently on Hold")
                return redirect('deposit')
            else:
                request.session['username'] = username
                request.session['amount'] = amount
                request.session['ref_no'] = ref_no
                request.session['t_amt'] = t_amt
                request.session['c_amt'] = c_amt
                context = {'username': username, 'email': email, 't_amt': t_amt,
                           'ref_no': ref_no, 'show': show, 'amount': amount,
                           'c_amt': c_amt, 'charge': charge, 'api_pk': api_pk,
                           'p_mode': p_mode}
            return render(request, 'deposit-money-confirm.html', context)
        elif p_mode == "monify":
            if status == "hold":
                messages.error(request, "You Account is Currently on Hold")
                return redirect('deposit')
            else:
                request.session['username'] = username
                request.session['amount'] = amount
                request.session['ref_no'] = ref_no
                request.session['t_amt'] = t_amt
                request.session['c_amt'] = c_amt
                context = {'username': username, 'email': email, 't_amt': t_amt,
                           'ref_no': ref_no, 'show': show, 'amount': amount,
                           'c_amt': c_amt, 'charge': charge, 'api_pk': api_pk,
                           'p_mode': p_mode}
                return render(request, 'monify.html', context)
    else:
        return render(request, 'deposit.html')


@login_required(login_url='login')
def payment(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    amount = request.session['amount']
    username = request.session['username']
    ref_no = request.session['ref_no']
    t_amt = request.session['t_amt']
    c_amt = request.session['c_amt']
    balance = Account.objects.values('bal').get(username=username)['bal']
    acct_bal = (float(balance))
    amt = (float(amount))
    new = acct_bal + amt
    Account.objects.filter(username=username).update(bal=new)
    email = User.objects.values('email').get(username=username)['email']
    first_name = User.objects.values('first_name').get(username=username)['first_name']
    t_reg = Transactions(sender='Card Deposit', receiver=username, amount=amount, description='Deposit',
                         ref_no=ref_no, cur="NGN", charges=c_amt)
    t_reg.save()
    subject, from_email, to = 'Fund Deposit', EMAIL_FROM, email
    html_content = render_to_string('mail/deposit.html',
                                    {'username': username, 'amount': amount,
                                     'new': new, 'ref_no': ref_no, 'date': now,
                                     'first_name': first_name, 't_amt': t_amt, 'c_amt': c_amt})
    text_content = strip_tags(html_content)
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()
    return render(request, 'deposit-money-success.html', {'amount': amount})


@login_required(login_url='login')
def fail(request):
    messages.error(request, 'Transaction Fail')
    return redirect('deposit')


@login_required(login_url='login')
def int_mode(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    if request.method == 'POST':
        user = request.POST['user']
        i_mode = request.POST['int_mode']
        Merchant.objects.filter(bus_owner_username=user).update(int_mode=i_mode)
        messages.success(request, 'Updated Successfully')
        return redirect('merchant')


@login_required(login_url='login')
def w_pay(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    if request.method == 'POST':
        user = request.POST['user']
        w_pay_charge = request.POST['w_pay']
        Merchant.objects.filter(bus_owner_username=user).update(w_p_charges=w_pay_charge)
        messages.success(request, 'Updated Successfully')
        return redirect('merchant')


def card(request):
    N = 16
    card_no = ''.join(random.choices(string.digits, k=N))
    P = 4
    pin = ''.join(random.choices(string.digits, k=P))
    c_user = request.user.username
    if request.method == 'POST':
        card_user = request.POST['username']
        if VirtualCard.objects.filter(card_user=card_user).exists():
            return redirect('dashboard')
        else:
            virtual = VirtualCard(card_user=card_user, card_no=card_no, pin=pin, card_bal='0.00')
            virtual.save()
        return redirect('dashboard')


@login_required(login_url='login')
def virtual_card(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    url = '/account/virtual_card'
    ref_no = uuid.uuid4().hex[:10].upper()
    if request.method == 'POST':
        s_username = request.POST['s_username']
        card_no = request.POST['card_no']
        amount = request.POST['amount']
        sender_bal = Account.objects.values('bal').get(username=s_username)['bal']
        card_bal = VirtualCard.objects.values('card_bal').get(card_no=card_no)['card_bal']
        status = Account.objects.values('status').get(username=s_username)['status']
        charge = Commission.objects.values('transfer').get(id=1)['transfer']
        f_charge = (float(charge))
        am = (float(amount))
        s_bal = (float(sender_bal))
        c_bal = (float(card_bal))
        c_amt = am * (f_charge / 100)
        if am > s_bal:
            messages.info(request, 'Insufficient Fund')
            return render(request, 'errors.html', {'url': url})
        elif status == 'hold':
            messages.warning(request, 'Your Account is on Hold. Contact Admin to rectify')
            return render(request, 'errors.html', {'url': url})
        else:
            new = (s_bal - (am + c_amt))
            Account.objects.filter(username=s_username).update(bal=new)

            new2 = c_bal + am
            VirtualCard.objects.filter(card_no=card_no).update(card_bal=new2)

            trans = Transactions(sender=s_username, receiver=card_no, amount=amount, ref_no=ref_no,
                                 description="E-Card TopUp", charges=0)
            trans.save()
            messages.success(request, 'Transaction Successful')
            return render(request, 'success.html', {'url': url})
    return render(request, 'virtual_card.html')


@login_required(login_url='login')
def check(request):
    if request.method == 'POST':
        card_no = request.POST['card_no']
        card_check = VirtualCard.objects.filter(card_no=card_no).exists()
        if card_check:
            check = VirtualCard.objects.all().get(card_no=card_no)
            name = VirtualCard.objects.values('card_user').get(card_no=card_no)['card_user']
            first = User.objects.values('first_name').get(username=name)['first_name']
            last = User.objects.values('last_name').get(username=name)['last_name']
            cont = {'check': check, 'card_check': card_check, 'first': first, 'last': last}
            return render(request, 'virtual_card.html', cont)
        else:
            messages.error(request, 'Card Not Found')
            return redirect('virtual_card')
    return render(request, 'virtual_card.html')


@login_required(login_url='login')
def customer(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    c_username = request.user.username
    c_user = request.user.id
    show = Customer.objects.filter(merchant_id=c_user)
    shows = Account.objects.all().get(username=c_username)
    vendor = Transactions.objects.filter(receiver=c_username).filter(
        description='Merchant').aggregate(Sum('amount'))['amount__sum']
    paginator = Paginator(show, 5)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    context = {'show': page_obj, 'shows': shows, 'vendor': vendor}
    return render(request, 'customer.html', context)


@login_required(login_url='login')
def add_customer(request):
    url = '/account/customer'
    c_user = request.user.id
    if request.method == 'POST':
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        email = request.POST['email']
        mobile_no = request.POST['mobile_no']
        if Customer.objects.filter(email=email).exists():
            messages.error(request, 'Customer Already Exist')
            return render(request, 'errors.html', {'url': url})
        elif Customer.objects.filter(phone_no=mobile_no).exists():
            messages.error(request, 'Mobile Number Already Exist')
            return render(request, 'errors.html', {'url': url})
        else:
            cus = Customer(merchant_id=c_user, first_name=first_name, last_name=last_name, email=email,
                           phone_no=mobile_no)
            cus.save()
            messages.success(request, 'Customer Added Successfully')
            return render(request, 'success.html', {'url': url})
    else:
        messages.error(request, 'Error Adding Customer')
        return render(request, 'errors.html', {'url': url})


@login_required(login_url='login')
def bulk_payment(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    c_username = request.user.username
    c_user = request.user.id
    show = BulkPayment.objects.filter(merchant_id=c_user).order_by('-id')
    shows = Account.objects.all().get(username=c_username)
    vendor = Transactions.objects.filter(receiver=c_username).filter(
        description='Merchant').aggregate(Sum('amount'))['amount__sum']
    paginator = Paginator(show, 5)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    context = {'shows': shows, 'show': page_obj, 'vendor': vendor}
    return render(request, 'bulk.html', context)


@login_required(login_url='login')
def add_bulk(request):
    c_username = request.user.username
    c_user = request.user.id
    url = '/account/bulk-payment'
    if request.method == 'POST':
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        email = request.POST['email']
        mobile_no = request.POST['mobile_no']
        amount = request.POST['amount']
        c_r_no = Account.objects.values('phone_no').get(username=c_username)['phone_no']
        if BulkPayment.objects.filter(email=email).exists():
            messages.error(request, 'Email Already Exist')
            return render(request, 'errors.html', {'url': url})
        elif BulkPayment.objects.filter(phone_no=mobile_no).exists():
            messages.error(request, 'Mobile Number Already Exist')
            return render(request, 'errors.html', {'url': url})
        elif c_r_no == mobile_no:
            messages.error(request, "You can't add Your own Number")
            return render(request, 'errors.html', {'url': url})
        else:
            cus = BulkPayment(merchant_id=c_user, first_name=first_name, last_name=last_name,
                              email=email, phone_no=mobile_no, amount=amount)
            cus.save()
            messages.success(request, 'Staff Added Successfully')
            return render(request, 'success.html', {'url': url})
    else:
        messages.error(request, 'Error Adding Staff')
        return render(request, 'errors.html', {'url': url})


@login_required(login_url='login')
def upload_csv(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    url = '/account/bulk-payment'
    c_user = request.user.id
    if request.method == 'POST':
        csv_file = request.FILES['file']
        if not csv_file.name.endswith('.csv'):
            messages.error(request, 'File not csv')
            return render(request, 'errors.html', {'url': url})

        decoded_file = csv_file.read().decode('utf-8')
        io_string = io.StringIO(decoded_file)
        # next(io_string)
        for data in csv.reader(io_string, delimiter=',', quotechar='|'):
            # print(data[1])
            BulkPayment.objects.update_or_create(merchant_id=c_user,
                                                 first_name=data[0],
                                                 last_name=data[1],
                                                 email=data[2],
                                                 phone_no=data[3],
                                                 amount=data[4])
        messages.success(request, 'Record Updated')
        return render(request, 'success.html', {'url': url})
    else:
        return render(request, 'upload_csv.html')


def delete_bulk(request, id):
    BulkPayment.objects.filter(id=id).delete()
    messages.success(request, 'Record Deleted')
    return redirect('staff')


@login_required(login_url='login')
def pay_bulks(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    ref_no = uuid.uuid4().hex[:10].upper()
    c_user = request.user.username
    if request.method == 'POST':
        month = request.POST['month']
        comment = request.POST['comment']
        status = Account.objects.values('status').get(username=c_user)['status']
        sender = Merchant.objects.values('bus_name').get(bus_owner_username=c_user)['bus_name']
        s_bal = Account.objects.values('bal').get(username=c_user)['bal']
        am = [float(bulks.amount) for bulks in BulkPayment.objects.all()]
        amt = (sum(am))
        sb = (float(s_bal))

        if status == 'hold':
            messages.error(request, 'Your Account is on Hold, Please contact our agent')
            return redirect('staff')
        else:
            for bulks in BulkPayment.objects.all():
                if Account.objects.filter(phone_no=bulks.phone_no).exists():
                    bal = Account.objects.values('bal').get(phone_no=bulks.phone_no)['bal']
                    r_username = Account.objects.values('username').get(phone_no=bulks.phone_no)['username']
                    # print(am)
                    amount = BulkPayment.objects.values('amount').get(phone_no=bulks.phone_no)['amount']
                    rb = (float(bal))
                    # print(amount)
                    new = rb + amount
                    Account.objects.filter(phone_no=bulks.phone_no).update(bal=new)

                    new2 = sb - amt
                    Account.objects.filter(username=c_user).update(bal=new2)

                    bulk = Transactions(sender=c_user, receiver=r_username, amount=bulks.amount,
                                        ref_no=ref_no, description=comment, charges=0)
                    bulk.save()
                    subject, from_email, to = 'Payment', EMAIL_FROM, bulks.email
                    html_content = render_to_string('mail/email.html', {'username': r_username, 'amount': bulks.amount,
                                                                        'comment': comment, 'bal': new, 'sender': sender,
                                                                        'rb': rb, 'ref': ref_no, 'date': now})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                else:
                    messages.warning(request, 'Some Users info is wrong')
            messages.success(request, 'Done')
        return redirect('staff')
    else:
        messages.error(request, 'Transaction Fail')
        return redirect('staff')


def pay_single(request, id):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    url = '/account/bulk-payment'
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    ref_no = uuid.uuid4().hex[:10].upper()
    c_user = request.user.username
    r_no = BulkPayment.objects.values('phone_no').get(id=id)['phone_no']
    # ch = Account.objects.values('phone_no').get(phone_no=r_no)['phone_no']
    if Account.objects.filter(phone_no=r_no).exists():
        amount = BulkPayment.objects.values('amount').get(id=id)['amount']
        r_no = BulkPayment.objects.values('phone_no').get(id=id)['phone_no']
        r_bal = Account.objects.values('bal').get(phone_no=r_no)['bal']
        s_bal = Account.objects.values('bal').get(username=c_user)['bal']
        s_no = Account.objects.values('phone_no').get(username=c_user)['phone_no']
        r_email = BulkPayment.objects.values('email').get(id=id)['email']
        s_email = User.objects.values('email').get(username=c_user)['email']
        r_username = Account.objects.values('username').get(phone_no=r_no)['username']
        r_status = Account.objects.values('status').get(phone_no=r_no)['status']
        s_status = Account.objects.values('status').get(username=c_user)['status']
        rb = (float(r_bal))
        sb = (float(s_bal))
        if amount > sb:
            messages.error(request, 'Insufficient Balance')
            return render(request, 'errors.html', {'url': url})
        elif r_status == 'hold':
            messages.error(request, 'User can not Recieve fun at the Moment')
            return render(request, 'errors.html', {'url': url})
        elif s_status == 'hold':
            messages.error(request, 'Sorry You Account is on hold')
            return render(request, 'errors.html', {'url': url})
        elif r_no == s_no:
            messages.error(request, "Sorry Can't Send money to yourseelf")
            return render(request, 'errors.html', {'url': url})
        else:
            # credit Receiver
            new = rb + amount
            Account.objects.filter(phone_no=r_no).update(bal=new)

            # Debit Sender
            new2 = sb - amount
            Account.objects.filter(username=c_user).update(bal=new2)

            Transactions.objects.create(sender=c_user,
                                        receiver=r_username,
                                        amount=amount,
                                        ref_no=ref_no,
                                        description='Payment', charges=0)

            # Receiver
            subject, from_email, to = 'Payment', EMAIL_FROM, r_email
            html_content = render_to_string('mail/email.html', {'username': r_username, 'amount': amount,
                                                                'comment': 'Payment', 'bal': new, 'sender': c_user,
                                                                'rb': rb, 'ref': ref_no, 'date': now})
            text_content = strip_tags(html_content)
            msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
            msg.attach_alternative(html_content, "text/html")
            msg.send()

            # Sender
            subject, from_email, to = 'Payment', EMAIL_FROM, s_email
            html_content = render_to_string('mail/remail.html', {'username': c_user, 'amount': amount,
                                                                 'comment': 'Payment', 'bal': new2, 'sender': r_username,
                                                                 'rb': sb, 'ref': ref_no, 'date': now})
            text_content = strip_tags(html_content)
            msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
            msg.attach_alternative(html_content, "text/html")
            msg.send()
            messages.success(request, 'Transaction Successful')
            return render(request, 'success.html', {'url': url})
    else:
        messages.error(request, 'User not found')
        return render(request, 'errors.html', {'url': url})


@login_required(login_url='login')
def receipt(request, id):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    c_user = request.user.username
    user = Merchant.objects.all().get(bus_owner_username=c_user)
    show = Transactions.objects.all().get(id=id)
    phone = Account.objects.values('phone_no').get(username=c_user)['phone_no']
    return render(request, 'receipt.html', {'show': show, 'user_p': user, 'phone': phone})


@login_required(login_url='login')
def group_payment(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    user_id = request.user.id
    c_user = request.user.username
    show = Account.objects.all().get(username=c_user)
    vendor = Transactions.objects.filter(receiver=c_user).filter(
        description='Merchant').aggregate(Sum('amount'))['amount__sum']
    all_group = PaymentGroup.objects.filter(group_creator_id=user_id).order_by('-id')
    paginator = Paginator(all_group, 8)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    return render(request, 'group.html', {'show': show, 'vendor': vendor, 'all_group': page_obj})


@receiver(user_logged_in)
def remove_other_sessions(sender, user, request, **kwargs):
    # remove other sessions
    Session.objects.filter(usersession__user=user).delete()

    # save current session
    request.session.save()

    # create a link from the user to the current session (for later removal)
    UserSession.objects.get_or_create(
        user=user,
        session=Session.objects.get(pk=request.session.session_key)
    )


@login_required(login_url='login')
def add_group(request):
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    user_id = request.user.id
    if request.method == "POST":
        group_name = request.POST['group_name']
        grp = PaymentGroup(group_creator_id=user_id, group_name=group_name, date_created=now)
        grp.save()
        messages.success(request, "Group Created Successfully")
        return redirect('group_payment')
    else:
        messages.error(request, "Operation Fail!")
        return redirect('group_payment')


@login_required(login_url='login')
def add_member(request):
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    user_id = request.user.id
    c_user_no = request.user.phone
    if request.method == "POST":
        group_id = request.POST['group_id']
        group_creator_id = request.POST['g_creator_id']
        mobile_no = request.POST['mobile_no']
        if mobile_no == c_user_no:
            messages.error(request, "You can't add yourself to the group")
            return redirect('group_payment')
        elif GroupMember.objects.filter(mobile_no=mobile_no).exists():
            messages.error(request, "User Already Exist")
            return redirect('group_payment')
        else:
            if UserModel.objects.filter(phone=mobile_no).exists():
                grp = GroupMember(group_id=group_id, group_creator_id=group_creator_id, mobile_no=mobile_no, date_added=now)
                grp.save()
                messages.success(request, "Member Added Successful")
                return redirect('group_payment')
            else:
                messages.error(request, "User is not a valid jCollect User")
                return redirect('group_payment')
    else:
        return redirect('group_payment')


def delete_mem(request, id):
    url = '/account/group_payment'
    GroupMember.objects.filter(id=id).delete()
    messages.success(request, "Member Deleted")
    return render(request, 'success.html', {'url': url})

@login_required(login_url='login')
def view_group_member(request, id):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    user_id = request.user.id
    c_user = request.user.username
    show = Account.objects.all().get(username=c_user)
    vendor = Transactions.objects.filter(receiver=c_user).filter(
        description='Merchant').aggregate(Sum('amount'))['amount__sum']
    group_name = PaymentGroup.objects.values('group_name').get(id=id)['group_name']
    all_member = GroupMember.objects.filter(group_id=id, group_creator_id=user_id).order_by('-id')
    paginator = Paginator(all_member, 5)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    return render(request, 'view_group_member.html', {'show': show, 'vendor': vendor, 'all_member': page_obj, 'group_name': group_name})


@login_required(login_url='login')
def pay_group(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    c_user_no = request.user.phone
    sender = request.user.username
    if request.method == "POST":
        group_id = request.POST['group_id']
        g_creator_id = request.POST['g_creator_id']
        amount = request.POST['amount']
        desc = request.POST['desc']
        am = float(amount)
        user_no = []
        for acc in GroupMember.objects.filter(group_id=group_id):
            user_no.append(acc.mobile_no)
        acrt_user = []
        for num in user_no:
            ref_no = uuid.uuid4().hex[:10].upper()
            status = Account.objects.values('status').get(phone_no=num)['status']
            if status == 'hold':
                continue
            acrt_user.append(num)
        un = len(acrt_user)
        final = am * un
        p_bal = Account.objects.values('bal').get(phone_no=c_user_no)['bal']
        newbal = (float(p_bal) - float(final))
        Account.objects.filter(phone_no=c_user_no).update(bal=newbal)
        s_name = UserModel.objects.all().get(phone=c_user_no)
        for phone in acrt_user:
            receiver = Account.objects.values('username').get(phone_no=phone)['username']
            bal = Account.objects.values('bal').get(phone_no=phone)['bal']
            ba = float(bal)
            new = ba + am
            Account.objects.filter(phone_no=phone).update(bal=new)

            tran = Transactions(sender=sender, receiver=receiver, amount=amount, ref_no=ref_no, date=now, description=desc, cur="NGN", charges='0')
            tran.save()
            subject = "Cash received"
            message = f'You just receive a payment of {am} from {s_name.last_name} {s_name.first_name}.'
            allP = UserModel.objects.all().get(phone=phone)
            subject, from_email, to = subject, EMAIL_FROM, allP.email
            html_content = render_to_string('mail/general_message.html',
                                        {'first_name': allP.first_name, 'message': message, 'subject': subject})
            text_content = strip_tags(html_content)
            msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
            msg.attach_alternative(html_content, "text/html")
            msg.send()
        messages.success(request, "Transaction Successful")
        return redirect('group_payment')


@require_POST
@csrf_exempt
def confirm_transfer(request):
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    ref_no = uuid.uuid4().hex[:10].upper()
    request_json = request.body.decode('utf-8')
    body = json.loads(request_json)
    trans_ref = body['transactionReference']
    amount = body['amountPaid']
    method = body['paymentMethod']
    email = body['customer']['email']
    receiver = body['customer']['name']
    sender = body['accountPayments'][0]['accountName']
    desc = body['paymentDescription']
    status = body['paymentStatus']
    t_type = body['product']['type']
    p_ref = body['paymentReference']
    print(desc)
    if t_type == "INVOICE":
        s_username = Invoice.objects.values('sender').get(inv_ref=p_ref)['sender']
        r_username = Invoice.objects.values('receiver').get(inv_ref=p_ref)['receiver']
        s_email = User.objects.values('email').get(username=s_username)['email']
        user_bal = Account.objects.values('bal').get(username=s_username)['bal']
        allP = Invoice.objects.all().get(inv_ref=p_ref)
        new = (float(user_bal) + float(amount))

        Account.objects.filter(username=s_username).update(bal=new)
        Invoice.objects.filter(inv_ref=p_ref).update(status=status, date_paid=now)

        create_trans_rec = Transactions(sender=r_username, receiver=s_username, amount=amount, ref_no=p_ref, date=now, 
        description=allP.cust_name + "|INVOICE|" + allP.sender_name, cur="NGN", charges="0")
        create_trans_rec.save()

        subject, from_email, to = 'Invoice', EMAIL_FROM, s_email
        html_content = render_to_string('mail/s_mail_invoice.html',
                                        {'r_username': allp.cust_name, 's_username': allP.sender_name,
                                         'amount': amount, 'ref_no': p_ref, 'new2': new, 'date': now})
        text_content = strip_tags(html_content)
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        return HttpResponse(request_json, status=200)
    else:
        user_name = UserModel.objects.all().get(email=email)
        bal = Account.objects.values('bal').get(username=user_name.username)['bal']

        new = (float(amount) + float(bal))
        Account.objects.filter(username=user_name.username).update(bal=new)

        tran = Transactions(sender=sender, receiver=user_name.username, amount=amount, ref_no=ref_no, 
        description=f'CR|{ref_no}|{sender}', cur='NGN', charges='0')
        tran.save()
        subject = "Fund Transfer"
        message = f'You just receive a Fund Transfer of {amount} from {sender}.'
        subject, from_email, to = subject, EMAIL_FROM, user_name.email
        html_content = render_to_string('mail/general_message.html',
                                        {'first_name': user_name.first_name, 'message': message, 'subject': subject})
        text_content = strip_tags(html_content)
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        return HttpResponse(request_json, status=200)


@login_required(login_url='login')
def kyc(request):
    c_user = request.user.username
    if request.method == "POST":
        address = request.POST['address']
        idm = request.POST['idm']
        idno = request.POST['idno']
        mid = request.FILES['mid']
        if address == "":
            messages.error(request, "Address can't be empty")
            return redirect('kyc')
        elif idm == "":
            messages.error(request, "Please select your means of ID")
            return redirect('kyc')
        elif idno == "":
            messages.error(request, "ID No can't be empty")
            return redirect('kyc')
        elif mid == "":
            messages.error(request, "Please Upload your Document")
            return redirect('kyc')
        else:
            ky = Kyc(
                c_username=request.user.username,
                address=address,
                idm=idm,
                idno=idno,
                mid=mid,
                status='0'
            )
            ky.save()
            return redirect('kyc')
    else:
        show1 = Kyc.objects.filter(c_username=c_user).exists()
        if show1:
            show = Kyc.objects.filter(c_username=c_user).exists()
            status = Kyc.objects.values('status').get(c_username=c_user)['status']
            return render(request, 'kyc.html', {'show': show, 'status': status})
        return render(request, 'kyc.html')


@login_required(login_url='login')
def bankTransfer(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    ref_no = uuid.uuid4().hex[:10].upper()
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    if request.method == "POST":
        amount = request.POST['amount']
        acct_num = request.POST['acct_num']
        bnk_code = request.POST['bnk_code']
        acct_name = request.POST['acct_name']
        s_username = request.POST['s_username']

        bals = Account.objects.all().get(username=s_username)
        if float(amount) > float(bals.bal):
            response = HttpResponse(status=401)
            return response
        else:
            url = "https://sandbox.monnify.com/api/v1/disbursements/single"

            payload = '{\n    \"amount\": '+ amount +',\n    \"reference\":\"'+ ref_no +'\",\n    \"narration\":\"Withdraw\",\n    \"bankCode\": \"' + bnk_code +'\",\n    \"accountNumber\": \"'+ acct_num +'\",\n    \"currency\": \"NGN\",\n    \"walletId\": \"654CAB2118124760A659C787B2AA38E8\"\n}'
            headers = {
            'Authorization': 'Basic TUtfVEVTVF84VUJYR0tURlNCOkVOUkM0RkRZS1NUVVlRS0E1M1lQWEJGTFVGWFdZSEcy',
            'Content-Type': 'application/json'
            }

            response = requests.request("POST", url, headers=headers, data = payload)

            r_dict = json.loads(response.text)

            resp = []
            for i in r_dict:
                bnk = r_dict[i]
                resp.append(bnk)
            res = resp[3]
            amount = res['amount']
            ref = res['reference']
            status = res['status']

            # Customer Info

            new = (float(bals.bal) - float(amount))
            Account.objects.filter(username=s_username).update(bal=new)
            tx = Transactions(sender=s_username, receiver=acct_name, amount=amount, ref_no=ref, date=now, description="BANK TRANSFER", charges="0", cur="NGN")
            tx.save()
            # Sender Email Notification
            messages.success(request, "Transaction Successfull")
            return HttpResponse()
    show = Banks.objects.filter()
    return render(request, 'bank.html', {'show': show})


@login_required(login_url='login')
def bankVerify(request):
    if request.method == "POST":
        acct_no = request.POST['acct_no']
        bank = request.POST['bank']
        amount = request.POST['amount']

        url = f"https://sandbox.monnify.com/api/v1/disbursements/account/validate?accountNumber={acct_no}&bankCode={bank}"

        payload = {}
        headers= {}

        response = requests.request("GET", url, headers=headers, data = payload)

        r_dict = json.loads(response.text)

        acc = []
        for i in r_dict:
            bnk = r_dict[i]
            acc.append(bnk)
        status = acc[1]
        if status == "success":
            ac_d = acc[3]
            acct_num = ac_d['accountNumber']
            acct_name = ac_d['accountName']
            bnk_code = ac_d['bankCode']

            bank_name = Banks.objects.values('name').get(code=bnk_code)['name']
            return render(request, 'bank.html', {'acct_num': acct_num, 'acct_name': acct_name, 'bnk_code': bnk_code, 
            'amount': amount, 'bank_name': bank_name, 'status': status})
        else:
            messages.error(request, "Invalid Account Details")
            return redirect('bankTransfer')


@login_required(login_url='login')
def fastPay(request):
    txt_id = uuid.uuid4().hex[:10].upper()
    if request.method == "POST":
        mobile = request.POST['mobile']
        amount = request.POST['amount']
        cur = request.POST['cur']
        charge = ((3.8/100) * float(amount))
        #print(charge)
        sc = ((1.2/100) * float(amount))
        t_charge = charge + sc
        total1 = float(amount) + t_charge
        #print(t_charge)
        userExist = UserModel.objects.filter(phone=mobile).exists()
        if userExist:
            show = Account.objects.all().get(phone_no=mobile)
            if cur == "USD":
                url = 'https://fcsapi.com/api-v2/forex/converter?symbol=USD/NGN&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                payload = {}
                headers = {
                'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                }

                response = requests.request("POST", url, headers=headers, data = payload)

                #print(response.text.encode('utf8'))
                r_dict = json.loads(response.text)
        
                e = []
                for i in r_dict:
                    dt = r_dict[i]
                    txn = e.append(dt)
                trans = e[3]
                total = trans['total']
                return render(request, 'fastpay.html', {'userExist': userExist, 'total': total, 'mobile': mobile, 'cur': cur, 'show': show, 'amount': amount, 'txt_id': txt_id})
            elif cur == "GBP":
                url = 'https://fcsapi.com/api-v2/forex/converter?symbol=GBP/NGN&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                payload = {}
                headers = {
                'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                }

                response = requests.request("POST", url, headers=headers, data = payload)

                #print(response.text.encode('utf8'))
                r_dict = json.loads(response.text)
        
                e = []
                for i in r_dict:
                    dt = r_dict[i]
                    txn = e.append(dt)
                trans = e[3]
                total = trans['total']
                return render(request, 'fastpay.html', {'userExist': userExist, 'total': total, 'mobile': mobile, 'cur': cur, 'show': show, 'amount': amount, 'txt_id': txt_id})
            elif cur == "EUR":
                url = 'https://fcsapi.com/api-v2/forex/converter?symbol=EUR/NGN&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                payload = {}
                headers = {
                'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                }

                response = requests.request("POST", url, headers=headers, data = payload)

                #print(response.text.encode('utf8'))
                r_dict = json.loads(response.text)
        
                e = []
                for i in r_dict:
                    dt = r_dict[i]
                    txn = e.append(dt)
                trans = e[3]
                total = trans['total']
                return render(request, 'fastpay.html', {'userExist': userExist, 'total': total, 'mobile': mobile, 'cur': cur, 'show': show, 'amount': amount, 'txt_id': txt_id})
        else:
            messages.error(request, "User with mobile Number not a member of jCollect")
            return redirect('fastPay')
    else:
        return render(request, 'fastpay.html')



@login_required(login_url='login')
def pay_success(request):
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M"))
    if request.method == "GET":
        txn_id = request.GET.get('paymentReference')
        mobile = request.GET.get('mobile')
        amt = request.GET.get('amount')
        localAmount = request.GET.get('localAmount')
        allDetail = UserModel.objects.all().get(phone=f'0{mobile}')

        url = "https://api.ravepay.co/flwv3-pug/getpaidx/api/v2/verify"

        payload = '{\n\t\"txref\" : \"' + txn_id + '\",\n\t\"SECKEY\" : \"FLWSECK_TEST-0b93cce86006c4f85d7af034d51169eb-X\"\n}'
        headers = {
        'Content-Type': 'application/json'
        }

        response = requests.request("POST", url, headers=headers, data = payload)

        #print(response.text.encode('utf8'))
        r_dict = json.loads(response.text)
        
        e = []
        for i in r_dict:
            dt = r_dict[i]
            txn = e.append(dt)
        trans = e[2]
        #print(trans)
        amot = trans['amount']
        cur = trans['currency']
        c_amount = trans['chargedamount']
        txref = trans['txref']
        txid = trans['txid']
        flwref = trans['flwref']
        f_charge = trans['appfee']
        chargecode = trans['chargecode']
        paymenttype = trans['paymenttype']
        narration = trans['narration']
        #(str(round(new1, 2)))
        sc = ((1.2/100) * float(localAmount))
        new_sc = (str(round(sc, 2)))
        init_amt = float(localAmount) - sc
        new_amt = (str(round(init_amt, 2)))

        if e[0] == "success":
            if chargecode == "00":
                if float(amot) == float(amt):
                    acctDetail = Bank.objects.all().get(username=allDetail.username)
                    url = "https://sandbox.monnify.com/api/v1/disbursements/single"

                    payload = '{\n    \"amount\": '+ new_amt +',\n    \"reference\":\"'+ txn_id +'\",\n    \"narration\":\"Withdraw\",\n    \"bankCode\": \"' + acctDetail.code +'\",\n    \"accountNumber\": \"'+ acctDetail.account_no +'\",\n    \"currency\": \"NGN\",\n    \"walletId\": \"654CAB2118124760A659C787B2AA38E8\"\n}'
                    headers = {
                    'Authorization': 'Basic TUtfVEVTVF84VUJYR0tURlNCOkVOUkM0RkRZS1NUVVlRS0E1M1lQWEJGTFVGWFdZSEcy',
                    'Content-Type': 'application/json'
                    }

                    response = requests.request("POST", url, headers=headers, data = payload)

                    r_dict = json.loads(response.text)

                    resp = []
                    for i in r_dict:
                        bnk = r_dict[i]
                        resp.append(bnk)
                    res = resp[3]
                    amount = res['amount']
                    ref = res['reference']
                    status = res['status']
                    txn = Transactions(sender=request.user.username, receiver=allDetail.username, amount=amount, ref_no=ref, date=now, description="FAST PAY", charges=new_sc, cur="NGN")
                    txn.save()
                    return render(request, 'fastpay_success.html', {'amount': amount, 'cur': "NGN", 'first_name': allDetail.first_name, 'last_name': allDetail.last_name})
                else:
                    messages.error(request, "Error Transaction")
                    return redirect('fastPay')
            else:
                messages.error(request, "Error Transaction")
                return redirect('fastPay')
        else:
            messages.error(request, "Error Transaction")
            return redirect('fastPay')


@login_required(login_url='login')
def mobileData(request):
    if Kyc.objects.filter(c_username=request.user.username).exists():
        if Kyc.objects.values('status').get(c_username=request.user.username)['status'] == False:
            messages.error(request, "PLease Kindly Update you KYC before you can use your Account")
            return redirect('dashboard')
    else:
        messages.warning(request, "PLease Kindly Update you KYC before you can use your Account")
        return redirect('kyc')
    if request.method == "POST":
        d_pack = request.POST['d_pack']
        get_queryset = Utility.objects.filter(service_id=d_pack)
        data = serializers.serialize('json', get_queryset)
        #print(data)
        return HttpResponse(data, content_type="application/json")
    else:
        return render(request, 'data.html')


@login_required(login_url='login')
def dataConfirm(request):
    txt_id = uuid.uuid4().hex[:10].upper()
    c_user_phone = request.user.phone
    c_user = request.user.username
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    c_email = request.user.email
    if request.method == "POST":
        d_pack = request.POST['d_pack']
        sel_pack = request.POST['sel_pack']
        m_no = request.POST['m_no']
        amount = Utility.objects.values('variation_amount').get(variation_code=sel_pack)['variation_amount']
        c_bal = Account.objects.all().get(phone_no=c_user_phone)

        url = "https://sandbox.vtpass.com/api/pay"

        payload = '{\n\t\"request_id\" : \"'+ txt_id +'\",\n\t\"serviceID\" : \"'+ d_pack +'\",\n\t\"billersCode\" : \"'+ m_no +'\",\n\t\"variation_code\" : \"'+ sel_pack +'\",\n\t\"amount\" : \"'+ str(amount) +'\",\n\t\"phone\" : \"'+ m_no +'\"\n}'
        headers = {
        'Authorization': 'Basic aWxlbW9iYXlvc2Ftc29uQGdtYWlsLmNvbToxMjM0NTY3OA==',
        'Content-Type': 'application/json',
        'Cookie': 'laravel_session=eyJpdiI6InZwdE5UdmNoYVhSVjZYQlJlcUVIQmc9PSIsInZhbHVlIjoieDBHc1BNaytJb29OM1BBOGg0c0lzM1dEa3NRRmRSSlk2TjBDZlcwZDNoREhNQkJrK0Z1VEpuckUydlpBWkJ1c001MHJpVGVOVWhrTklwYkRra3BjOHc9PSIsIm1hYyI6IjdjNTQ2N2UwNDFiNmFmZDNiM2ZjZTlkYjBkOGVjY2YyYWM5MTcxNjhjMGQ2NTlkZDg2NzI0ODNjOWUzMWE1NDEifQ%3D%3D'
        }

        response = requests.request("POST", url, headers=headers, data = payload)

        #print(response.text.encode('utf8'))
        d_dict = json.loads(response.text)
        f = []
        for j in d_dict:
            dat = d_dict[j]
            data_ = f.append(dat)
        data_resp = f[1]
        #print(data_resp)
        p_descr = data_resp['transactions']['product_name']
        trans_type = data_resp['transactions']['type']
        txf = data_resp['transactions']['transactionId']
        status = data_resp['transactions']['status']
        mno = data_resp['transactions']['unique_element']
        if status == "delivered":
            new = (float(c_bal.bal) - float(amount))
            Account.objects.filter(phone_no=c_user_phone).update(bal=new)
            txn = Transactions(sender=c_user, receiver="Mobile Data", amount=amount, ref_no=txf, date=now, description=f'{p_descr}|INTERNET DATA', charges='0', cur="NGN")
            txn.save()
            subject = "Internet Data"
            message = "Your Internet data was successful"

            subject, from_email, to = subject, EMAIL_FROM, c_email
            html_content = render_to_string('mail/general_message.html',
                                        {'first_name': c_bal.first_name, 'message': message, 'subject': subject})
            text_content = strip_tags(html_content)
            msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
            msg.attach_alternative(html_content, "text/html")
            msg.send()
            return HttpResponse()
        else:
            response = HttpResponse(status=401)
            return response


@login_required(login_url='login')
def airtimeRecharge(request):
    c_first_name = request.user.first_name
    c_user = request.user.phone
    c_username = request.user.username
    c_email = request.user.email
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    txt_id = uuid.uuid4().hex[:10].upper()
    if request.method == "POST":
        amount = request.POST['amount']
        mobile = request.POST['mobile']
        network = request.POST['network']
        code = "+234"
        mb = code + mobile[1:]
        amt = float(amount)
        act = Account.objects.all().get(phone_no=c_user)
        new_bal = float(act.bal) - amt
        if amt > float(act.bal):
            response = HttpResponse(status=401)
            return response
        else:
            url = "https://sandbox.vtpass.com/api/pay"

            payload = '{\n\t\"request_id\" : \"'+ txt_id +'\",\n\t\"serviceID\" : \"'+ network +'\",\n\t\"amount\" : \"'+ amount +'\",\n\t\"phone\" : \"'+ mobile +'\"\n}'
            headers = {
            'Authorization': 'Basic aWxlbW9iYXlvc2Ftc29uQGdtYWlsLmNvbToxMjM0NTY3OA==',
            'Content-Type': 'application/json',
            'Cookie': 'laravel_session=eyJpdiI6IkYrQUpSMXR2SndhNGphYVA5M0hhUXc9PSIsInZhbHVlIjoiUHdzdUlwUlBnWXh2RmN0Y3lDalIwV0dmY21Nd3JZMEFIMm9wRmFoRUVua044aUYranBBWmpCemlWSWxiN0RiZyswcWpxRDQ3allGMG0rOXBIYVpST2c9PSIsIm1hYyI6IjFhZjA5MWUwNTRmODY4YWQ2MDcxZjViYTMwNTc4MjE0OWJhMWYxZWM1NjIzNDNlZDZkOGFkMjAxZGQ3YTEwYmIifQ%3D%3D'
            }

            response = requests.request("POST", url, headers=headers, data = payload)
            #print(response.text.encode('utf8'))
            d_dict = json.loads(response.text)
            f = []
            for j in d_dict:
                airt = d_dict[j]
                airtime = f.append(airt)
            air_resp = f[1]
            #print(air_resp)
            status = air_resp['transactions']['status']
            p_descr = air_resp['transactions']['product_name']
            mob = air_resp['transactions']['unique_element']
            typ = air_resp['transactions']['type']
            txf = air_resp['transactions']['transactionId']
            if status == "delivered":
                Account.objects.filter(phone_no=c_user).update(bal=new_bal)
                txn = Transactions(sender=c_username, receiver="Airtime purchase", amount=amount, ref_no=txf, date=now, description=f'{p_descr}', charges='0', cur="NGN")
                txn.save()
                subject = "Airtime"
                message = "Your Airtime purchase was successful"

                subject, from_email, to = subject, EMAIL_FROM, c_email
                html_content = render_to_string('mail/general_message.html',
                                        {'first_name': act.first_name, 'message': message, 'subject': subject})
                text_content = strip_tags(html_content)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
                return HttpResponse()
            else:
                response = HttpResponse(status=401)
                return response
    else:
        return render(request, 'recharge.html')



@login_required(login_url='login')
def tvPayment(request):
    if request.method == "POST":
        sno = request.POST['sno']
        biller = request.POST['biller']
        url = "https://sandbox.vtpass.com/api/merchant-verify"

        payload = '{\n\t\n\t\"billersCode\" : '+ sno +',\n\t\"serviceID\" : \"'+ biller +'\"\n}'
        headers = {
        'Authorization': 'Basic aWxlbW9iYXlvc2Ftc29uQGdtYWlsLmNvbToxMjM0NTY3OA==',
        'Content-Type': 'application/json',
        'Cookie': 'laravel_session=eyJpdiI6IkFQYW1NZExRblhrQWhnMDRMb2IwY3c9PSIsInZhbHVlIjoiTnVrMURUeFIwbTlLeUpMZ25jTTB0eGkzeDVaZ1ZnQkRWMzFJS0lFQ3JqTGx3NzRSMXNxczdHVGlGS0xJRkFLbVF1akhxMms3TndwZUtLc0x1K205d3c9PSIsIm1hYyI6IjZkYTA3NDliOGE0OGZkNWY2MTliNTNlM2EzOWQyYjYyYmMyOWEyNGI4ZDA5NWZmMmNhNjJjZWYxYjU5NWIxNDEifQ%3D%3D'
        }

        response = requests.request("POST", url, headers=headers, data = payload)

        #print(response.text.encode('utf8'))
        d_dict = json.loads(response.text)
        f = []
        for j in d_dict:
            tv = d_dict[j]
            s_tv = f.append(tv)
        tv_resp = f[1]
        #print(tv_resp)
        status_code = f[0]
        bill = "startimes"
        if int(sno) or sno in tv_resp.values():
            cus_name = tv_resp['Customer_Name']
            #print(sno in tv_resp.values())
            if biller == bill:
                cus_id = tv_resp['Smartcard_Number']
                show = Utility.objects.filter(service_id=biller)
                return render(request, 'tv.html', {'status_code': status_code, 'cus_name': cus_name, 'sno': sno, 'cus_id': cus_id, 'show': show})
            elif cus_name in tv_resp.values():
                cus_id = tv_resp['Customer_ID']
                show = Utility.objects.filter(service_id=biller)
                return render(request, 'tv.html', {'status_code': status_code, 'cus_name': cus_name, 'sno': sno, 'cus_id': cus_id, 'show': show})
            else:
                return redirect('tvPayment')
        else:
            messages.error(request, "Invalid")
            return redirect('tvPayment')
    else:
        return render(request, 'tv.html')


@login_required(login_url='login')
def tvConfirm(request):
    txt_id = uuid.uuid4().hex[:10].upper()
    c_user_phone = request.user.phone
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M"))
    c_email = request.user.email
    c_username = request.user.username
    c_user_name = request.user.first_name
    if request.method == "POST":
        variation_code = request.POST['variation_code']
        sno = request.POST['sno']
        amount = Utility.objects.values('variation_amount').get(variation_code=variation_code)['variation_amount']
        service_id = Utility.objects.values('service_id').get(variation_code=variation_code)['service_id']
        amt = float(amount)
        act_bal = Account.objects.values('bal').get(phone_no=c_user_phone)['bal']
        new_bal = float(act_bal) - amt
        if amt > float(act_bal):
            response = HttpResponse(status=401)
            return response
        else:
            url = "https://sandbox.vtpass.com/api/pay"

            payload = '{\n\t\n\t\"request_id\" : \"'+ txt_id +'\",\n\t\"serviceID\" : \"'+ service_id +'\",\n\t\"billersCode\" : \"'+ sno +'\",\n\t\"variation_code\": \"'+ variation_code +'\",\n\t\"amount\" : \"'+ str(amount) +'\",\n\t\"phone\": \"'+ c_user_phone +'\"\n}'
            headers = {
            'Authorization': 'Basic aWxlbW9iYXlvc2Ftc29uQGdtYWlsLmNvbToxMjM0NTY3OA==',
            'Content-Type': 'application/json',
            'Cookie': 'laravel_session=eyJpdiI6ImJOMSs1MHpYejlYeEQyV01TSEVSRWc9PSIsInZhbHVlIjoidE9KaDc4MFFQYVkyS3duTVBHQXVsVHpsRlcwbHV0SG5FZ2pJOHVtY3RMd3BJWGM4QW5RdU5rQ1hzSzQ5UFVodzRhUVE0RGFVeEFxK0dPUktqV1BSa0E9PSIsIm1hYyI6ImFhZGMzMDY0OGIyYmViM2QwNDAyMDYxOWMwYWIwOGM2MmYxYzZhN2IzYzE4YzcyOTkzY2JjOTU5NTIzNDViZGUifQ%3D%3D'
            }

            response = requests.request("POST", url, headers=headers, data = payload)

            #print(response.text.encode('utf8'))
            d_dict = json.loads(response.text)
            f = []
            for j in d_dict:
                airt = d_dict[j]
                airtime = f.append(airt)
            tv_resp = f[1]['transactions']
            status = tv_resp['status']
            p_name = tv_resp['product_name']
            cno = tv_resp['unique_element']
            desc = tv_resp['type']
            trans_id = tv_resp['transactionId']
            if status == "delivered":
                Account.objects.filter(phone_no=c_user_phone).update(bal=new_bal)
                txn = Transactions(sender=c_username, receiver="TV Payment", amount=amount, ref_no=txt_id, date=now, description=f'{desc}', charges='0', cur="NGN")
                txn.save()

                # Sender Email Notification
                subject = "TV Payment"
                message = "Your TV Payment Was Successful"

                subject, from_email, to = subject, EMAIL_FROM, c_email
                html_content = render_to_string('mail/general_message.html',
                                        {'first_name': c_user_name, 'message': message, 'subject': subject})
                text_content = strip_tags(html_content)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
                return HttpResponse()
            else:
                response = HttpResponse(status=401)
                return response


@login_required(login_url='login')
def power(request):
    if request.method == "POST":
        plan = request.POST['plan']
        sub = request.POST['sub']
        m_no = request.POST['m_no']

        if plan == "":
            messages.error(request, "Please select your electricity plan")
            return redirect('power')
        elif sub == "":
            messages.error(request, "Please select your electricity subscriber")
            return redirect('power')
        
        else:
            url = "https://sandbox.vtpass.com/api/merchant-verify"

            payload = '{\n\t\n\t\"billersCode\" : '+ m_no +',\n\t\"serviceID\" : \"'+ sub +'\",\n\t\"type\": \"'+ plan +'\"\n}'
            headers = {
            'Authorization': 'Basic aWxlbW9iYXlvc2Ftc29uQGdtYWlsLmNvbToxMjM0NTY3OA==',
            'Content-Type': 'application/json',
            'Cookie': 'laravel_session=eyJpdiI6Im1vanQ3OTI5V2NpQWVSUG1nYTZIbWc9PSIsInZhbHVlIjoibWFTclwvUW5ibnFVb29iUTRFa0MxdjBwN1U0ZlduRjhmMmF3XC9XbWFZK21HN1VJQkM0YmRIWkZlQUh4bU9halhabjdnUzI5YWI3R3FjZEQ5TGtrQWd3UT09IiwibWFjIjoiNTQyODA4NzllODM4MTIwZTdkNzY0Nzg4ZTU5ZjI2Yzg3N2EwMzdjZWVmNGM4NDQ0MDdkNmFlMTlhMTI4NWI4MSJ9'
            }

            response = requests.request("POST", url, headers=headers, data = payload)

            #print(response.text.encode('utf8'))
            d_dict = json.loads(response.text)
            f = []
            for j in d_dict:
                tv = d_dict[j]
                s_tv = f.append(tv)
            p_resp = f[1]
            print(p_resp)
            if 'error' in p_resp:
                error = p_resp['error']
                messages.error(request, error)
                return redirect('power')
            else:
                if int(m_no) or m_no in p_resp.values():
                    cus_name = p_resp['Customer_Name']
                    if sub == "ibadan-electric" or sub == "jos-electric" or sub == "portharcourt-electric":
                        cus_name = p_resp['Customer_Name']
                        MeterNumber = p_resp['MeterNumber']
                        address = p_resp['Address']
                        return render(request, 'power.html', {'cus_name': cus_name, 'm_no': MeterNumber, 'address': address, 'plan': plan, 'sub': sub})
                    elif p_resp['Customer_Name'] in p_resp.values():
                        m_no = p_resp['Meter_Number'] 
                        cus_dist = p_resp['Customer_District']
                        address = p_resp['Address']
                        return render(request, 'power.html', {'cus_name': cus_name, 'm_no': m_no, 
                        'cus_dist': cus_dist, 'address': address, 'plan': plan, 'sub': sub})
                    else:
                        error = p_resp['error']
                        messages.error(request, error)
                        return redirect('power')
    else:
        return render(request, 'power.html')


@login_required(login_url='login')
def powerConfirm(request):
    txt_id = uuid.uuid4().hex[:10].upper()
    c_user_phone = request.user.phone
    c_username = request.user.username
    c_user_name = request.user.last_name
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M"))
    c_email = request.user.email
    if request.method == "POST":
        plan = request.POST['plan']
        sub = request.POST['sub']
        amount = request.POST['amount']
        m_no = request.POST['m_no']
        c_bal = Account.objects.all().get(phone_no=c_user_phone)

        if amount < "500":
            response = HttpResponse(status=401)
            return response
        else:
            url = "https://sandbox.vtpass.com/api/pay"

            payload = '{\n\t\n\t\"request_id\" : \"'+ txt_id +'\",\n\t\"serviceID\" : \"'+ sub +'\",\n\t\"billersCode\" : \"'+ m_no +'\",\n\t\"variation_code\": \"'+ plan +'\",\n\t\"amount\" : \"'+ amount +'\",\n\t\"phone\": \"'+ c_user_phone +'\"\n}'
            headers = {
            'Authorization': 'Basic aWxlbW9iYXlvc2Ftc29uQGdtYWlsLmNvbToxMjM0NTY3OA==',
            'Content-Type': 'application/json',
            'Cookie': 'laravel_session=eyJpdiI6InZKWlwvMU5DYkxBV0NCNGI2dW1BXC9KQT09IiwidmFsdWUiOiJIbVRZTkkxYTh3ZXk4QzZnQkM2MCtZbVBtRkdha2tVYlVTNk9kWndoZXpVTlZmeGxRT3R2MkUzcWdERlFiQlBSeTBIWFhtb2dJdHB5NnhmdzdDUHp1dz09IiwibWFjIjoiMWEyNTVkNDk2YmQ4ZmU5M2MzOGE1YjJlZjZiM2Y4MDUxMGJmNWY4MDZkYjA4ZjExMjYxMDM1YTYzMzNkYmE3NiJ9'
            }

            response = requests.request("POST", url, headers=headers, data = payload)
            #print(response.text.encode('utf8'))
            d_dict = json.loads(response.text)
            f = []
            for j in d_dict:
                po = d_dict[j]
                s_po = f.append(po)
            po_resp = f[1]
            status = po_resp['transactions']['status']
            desc = po_resp['transactions']['product_name']
            tran_id = po_resp['transactions']['transactionId']
            #print(po_resp)
            code = f[6]
            #print(plan)
            if status == "delivered":
                if plan == "postpaid":
                    new = (float(c_bal.bal) - float(amount))
                    Account.objects.filter(phone_no=c_user_phone).update(bal=new)
                    txn = Transactions(sender=c_username, receiver="Electricity Bill", amount=amount, ref_no=txt_id, date=now, description=f'{desc}', charges='0', cur="NGN")

                    elect = Electricity(owner_username=c_username, txn_id=txt_id, txn_ref=tran_id, desc=desc, 
                    token="POSTPAID", date_purchase=now)
                    txn.save()
                    elect.save()

                    #Buyer/User Email 
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_email
                    html_content = render_to_string('mail/elect.html',
                                    {'last_name': c_user_name, 'date': now, 'reciever': m_no, 'amt': amount, 'd_pack': sub, 'trans_ref': tran_id})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                #Sending of the Electricty Token

                    subject, from_email, to = 'Electricity PIN/Token', EMAIL_FROM, c_email
                    html_content = render_to_string('mail/elect2.html',
                                    {'last_name': c_user_name, 'date': now, 'reciever': m_no, 'amt': amount, 'd_pack': sub, 'trans_ref': tran_id, 'token': code})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return HttpResponse()
                else:
                    new = (float(c_bal.bal) - float(amount))
                    Account.objects.filter(phone_no=c_user_phone).update(bal=new)
                    txn = Transactions(sender=c_username, receiver="Electricity Bill", amount=amount, ref_no=txt_id, date=now, description=f'{desc}', charges='0', cur="NGN")

                    elect = Electricity(owner_username=c_username, txn_id=txt_id, txn_ref=tran_id, desc=desc, 
                    token=code, date_purchase=now)
                    txn.save()
                    elect.save()

                    #Buyer/User Email 
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_email
                    html_content = render_to_string('mail/elect.html',
                                    {'last_name': c_user_name, 'date': now, 'reciever': m_no, 'amt': amount, 'd_pack': sub, 'trans_ref': tran_id})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #Sending of the Electricty Token

                    subject, from_email, to = 'Electricity PIN/Token', EMAIL_FROM, c_email
                    html_content = render_to_string('mail/elect2.html',
                                    {'last_name': c_user_name, 'date': now, 'reciever': m_no, 'amt': amount, 'd_pack': sub, 'trans_ref': tran_id, 'token': code})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return HttpResponse()
            else:
                response = HttpResponse(status=401)
                return response

