from djoser.serializers import UserCreateSerializer, UserSerializer
from rest_framework import serializers
from .models import *
from account.models import *


class QuerySerializers(serializers.ModelSerializer):
    class Meta:
        model = Transactions
        fields = "__all__"


class TransactionsSerializers(serializers.ModelSerializer):
    class Meta:
        model = Transactions
        fields = "__all__"