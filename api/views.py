from django.shortcuts import render
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth import get_user_model, authenticate, login as dj_login, logout as s_logout
from rest_framework import status
from rest_framework.response import Response
from django.db.models import Q
from .serializers import *
from datetime import datetime
import random
import string
import uuid
import math
import requests
import json
from django.contrib.auth import get_user_model
from account.models import *
from super.models import *
from django.contrib.auth import user_logged_in
from django.dispatch.dispatcher import receiver
UserModel = get_user_model()
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.core.paginator import Paginator
from django.utils.html import strip_tags
from django.db.models import Sum
from wallet.settings import EMAIL_FROM


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def query(request, ref):
    try:
        show = Transactions.objects.all().get(ref_no=ref)
    except Transactions.DoesNotExist:
        error = {
            "status": status.HTTP_400_BAD_REQUEST,
            "data": {
                "message": "Transaction doesn't exist"
            }
        }
        return Response(data=error, status=status.HTTP_400_BAD_REQUEST)

    da = {
        "status": {
            "status": status.HTTP_200_OK
        },
        "data": {
        "refNo": show.ref_no,
        "payment_date": show.date,
        "sender": show.sender,
        "receiver": show.receiver,
        "description": show.description,
        "charges": show.charges,
        "currency": show.cur,
        "amount": show.amount,
        }
    }
    return Response(data=da, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def pay(request):
    ref_no = uuid.uuid4().hex[:10].upper()
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M"))
    if request.method == "POST":
        amount = request.data.get('amount')
        merchant = request.data.get('merchant')
        phone = request.data.get('phone')
        password = request.data.get('password')
        callback = request.data.get('callback')

        show_merchant = Merchant.objects.values('bus_name').get(api_test_key=merchant)['bus_name']
        m_username = Merchant.objects.values('bus_owner_username').get(api_test_key=merchant)['bus_owner_username']
        mode = Merchant.objects.values('w_p_charges').get(api_test_key=merchant)['w_p_charges']
        charge = Commission.objects.values('merchant').get(id=1)['merchant']
        f_charge = (float(charge))
        am = (float(amount))
        c_amt = am * (f_charge / 100)


        user = authenticate(phone=phone, password=password)

        if user is not None:
            #dj_login(request, user)
            p_user = UserModel.objects.all().get(phone=phone)
            merchant = UserModel.objects.all().get(username=m_username)
            #print(merchant_email)
            payee = Account.objects.values('bal').get(phone_no=phone)['bal']
            payer = Account.objects.values('bal').get(username=m_username)['bal']
            merchant_name = Merchant.objects.values('bus_name').get(bus_owner_username=m_username)['bus_name']
            int_mode = Merchant.objects.values('int_mode').get(bus_owner_username=m_username)['int_mode']
            charge = Commission.objects.values('merchant').get(id=1)['merchant']
            mode = Merchant.objects.values('w_p_charges').get(bus_owner_username=m_username)['w_p_charges']
            f_charge = (float(charge))
            sb = (float(payee))
            rb = (float(payer))
            am = (float(amount))
            c_amt = am * (f_charge / 100)

            if am > sb:
                error = {
                    "status": status.HTTP_402_PAYMENT_REQUIRED,
                    "data": {
                        "message": "Insufficient balance"
                    }
                }
                return Response(data=error, status=status.HTTP_400_BAD_REQUEST)
            elif m_username == p_user.username:
                error = {
                    "status": status.HTTP_400_BAD_REQUEST,
                    "data": {
                        "message": "You Can't Send Money to Yourself!!"
                    }
                }
                return Response(data=error, status=status.HTTP_400_BAD_REQUEST)

            else:
                if int_mode == 'live':
                    if mode == 'merchant':
                        c_charge = am - c_amt
                        new = rb + c_charge
                        Account.objects.filter(username=m_username).update(bal=new)

                        new_2 = sb - am
                        Account.objects.filter(phone_no=phone).update(bal=new_2)

                        data = {
                            "ref_no": ref_no,
                            "date": now,
                            "sender": p_user.username,
                            "receiver": m_username,
                            "description": "Merchant",
                            "charges": c_amt,
                            "cur": "NGN",
                            "amount": c_charge,
                            }

                        resp = {
                            "status": {
                                "status": status.HTTP_200_OK
                            },
                            "data": {
                                "refNo": ref_no,
                                "date": now,
                                "sender": p_user.first_name + ' ' + p_user.last_name,
                                "receiver": merchant_name,
                                "description": "Merchant",
                                "charges": c_amt,
                                "currency": "NGN",
                                "amount": c_charge,
                                }
                            }

                        tx = TransactionsSerializers(data=data)
                        if tx.is_valid():
                            tx.save()
    
                        #Payee Email
                        subject, from_email, to = 'Purchase', EMAIL_FROM, p_user.email
                        html_content = render_to_string('mail/vendor_payee.html',
                                                        {'username': p_user.first_name, 'amount': amount, 'ref_no': ref_no,
                                                         'merchant': merchant_name, 'new': new_2})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()

                        #Merchant Email
                        subject, from_email, to = 'Purchase', EMAIL_FROM, merchant.email
                        html_content = render_to_string('mail/vendor.html',
                                                        {'username': p_user.first_name, 'amount': amount, 'ref_no': ref_no,
                                                         'merchant': merchant_name, 'new': new})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()
                        return Response(data=resp, status=status.HTTP_200_OK)

                    else:
                        new = rb + am
                        Account.objects.filter(username=m_username).update(bal=new)

                        c_charge = am + c_amt
                        new_2 = sb - c_charge
                        Account.objects.filter(phone_no=phone).update(bal=new_2)


                        data = {
                            "ref_no": ref_no,
                            "date": now,
                            "sender": p_user.username,
                            "receiver": m_username,
                            "description": "Merchant",
                            "charges": c_amt,
                            "cur": "NGN",
                            "amount": amount,
                            }

                        resp = {
                            "status": {
                                "status": status.HTTP_200_OK
                            },
                            "data": {
                                "refNo": ref_no,
                                "date": now,
                                "sender": p_user.first_name + ' ' + p_user.last_name,
                                "receiver": merchant_name,
                                "description": "Merchant",
                                "charges": c_amt,
                                "currency": "NGN",
                                "amount": amount,
                                }
                            }

                        tx = TransactionsSerializers(data=data)
                        if tx.is_valid():
                            tx.save()

                        
                        # Payee Email
                        subject, from_email, to = 'Purchase', EMAIL_FROM, p_user.email
                        html_content = render_to_string('mail/vendor_payee.html',
                                                        {'username': p_user.first_name, 'amount': amount, 'ref_no': ref_no,
                                                         'merchant': merchant_name, 'new': new_2})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()

                        # Merchant Email
                        subject, from_email, to = 'Purchase', EMAIL_FROM, merchant.email
                        html_content = render_to_string('mail/vendor.html',
                                                        {'username': p_user.first_name, 'amount': amount, 'ref_no': ref_no,
                                                         'merchant': merchant_name, 'new': new})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()
                        return Response(data=resp, status=status.HTTP_200_OK)
                else:
                    resp = {
                        "status": {
                            "status": status.HTTP_200_OK
                        },
                        "data": {
                            "refNo": ref_no,
                            "payment_date": now,
                            "sender": p_user.first_name + ' ' + p_user.last_name,
                            "receiver": merchant_name,
                            "description": "Merchant",
                            "currency": "NGN",
                            "amount": amount,
                            }
                        }
                    # Payee Email
                    subject, from_email, to = 'Purchase', EMAIL_FROM, p_user.email
                    html_content = render_to_string('mail/vendor_payee.html',
                                                    {'username': p_user.first_name, 'amount': amount, 'ref_no': ref_no,
                                                     'merchant': merchant_name})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    # Merchant Email
                    subject, from_email, to = 'Purchase', EMAIL_FROM, merchant.email
                    html_content = render_to_string('mail/vendor.html',
                                                    {'username': p_user.first_name, 'amount': amount, 'ref_no': ref_no,
                                                     'merchant': merchant_name})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return Response(data=resp, status=status.HTTP_200_OK)
        else:
            error = {
                 "status": status.HTTP_400_BAD_REQUEST,
                "data": {
                    "message": "Invalid Payer Credentials"
                }
            }
            return Response(data=error, status=status.HTTP_400_BAD_REQUEST)