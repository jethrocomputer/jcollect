from django.urls import path, include
from api import views


urlpatterns = [
    path('', include('djoser.urls')),
    path('', include('djoser.urls.authtoken')),
    path('query/<ref>', views.query, name='query'),
    path('pay', views.pay, name='pay'),
]