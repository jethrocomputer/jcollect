from rest_framework import status
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response
from rest_framework.decorators import api_view
from account.models import Merchant, Account
from account.models import MerchantPayment
from super.models import *
from account.api.serializers import MerchantSerializer, MerchantPaymentSerializer


@api_view(['GET', ])
def api_account_view(request, id):
    try:
        merchant = Merchant.objects.get(id=id)
    except Merchant.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = MerchantSerializer(merchant)
        return Response(serializer.data)


# @api_view(['POST', ])
# @csrf_exempt
# def pay(request, id):
#     try:
#         merchant = MerchantPayment.objects.get(id=id)
#     except Merchant.DoesNotExist:
#         return Response(status=status.HTTP_404_NOT_FOUND)
#
#     try:
#         bal = Account.objects.get(id=id)
#         Account.objects.filter(id=id).update(bal='2000.0')
#     except Merchant.DoesNotExist:
#         return Response(status=status.HTTP_404_NOT_FOUND)
#     if request.method == 'POST':
#         serializer = MerchantPaymentSerializer(bal, data=request.data)
#
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#

@api_view(['POST'])
def pay(request):
        desc = request.data.get('item_name')
        amount = request.data.get('amount')
        merchant = request.data.get('merchant')
        success_url = request.data.get('success')
        show_merchant = Merchant.objects.values('bus_name').get(api_test_key=merchant)['bus_name']
        m_username = Merchant.objects.values('bus_owner_username').get(api_test_key=merchant)['bus_owner_username']
        logo = Merchant.objects.values('bus_logo').get(api_test_key=merchant)['bus_logo']
        mode = Merchant.objects.values('w_p_charges').get(api_test_key=merchant)['w_p_charges']
        charge = Commission.objects.values('merchant').get(id=1)['merchant']
        f_charge = (float(charge))
        am = (float(amount))
        c_amt = am * (f_charge / 100)
        context = {'desc': desc, 'amount': amount, 'show_merchant': show_merchant, 'm_username': m_username,
                   'success_url': success_url, 'logo': logo, 'c_amt': c_amt, 'mode': mode}
        return render(request, 'pay.html', context)
